            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Halaman Kriteria</h3>
                </div>
                
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4 class="card-title">List Data Kriteria</h4>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <button type="button" class="btn btn-success btn-rounded" data-toggle="modal" data-target="#modal_range_hasil">
                                            <i class="fa fa-crosshairs"></i> Target Nilai
                                        </button>&nbsp;&nbsp;&nbsp;
                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data">
                                            <i class="fa fa-plus-circle"></i> Tambah Kriteria
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kriteria</th>
                                                <th>Nilai Minimum</th>
                                                <th>Nilai Maximum</th>
                                                <th>Status Aktive</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $no = 1;
                                                if($list_kriteria){
                                                    foreach ($list_kriteria as $r_list_kriteria => $v_list_kriteria){
                                                        $tipe_kri = "cost";
                                                        if($v_list_kriteria->tipe_kri == 1){
                                                            $tipe_kri = "benefit";
                                                        }

                                                        $str_sts_active = "tidak aktif";
                                                        $btn_sts_active = "<button type=\"button\" class=\"btn btn-success\" id=\"active_kri\" onclick=\"get_active_kri('".$v_list_kriteria->id_kri."')\"><i class=\"fa fa-check-square-o\"></i></button>";
                                                        if($v_list_kriteria->sts_active == "0"){
                                                            $str_sts_active = "aktif";
                                                            
                                                            $btn_sts_active = "<button type=\"button\" class=\"btn btn-warning\" id=\"non_active_kri\" onclick=\"get_non_active_kri('".$v_list_kriteria->id_kri."')\"><i class=\"fa fa-window-close-o\"></i></button>";
                                                        }
                                                        print_r("<tr>
                                                                <td>".$no++."</td>
                                                                <td>".$v_list_kriteria->ket_kri."</td>
                                                                <td>".$v_list_kriteria->min."</td>
                                                                <td>".$v_list_kriteria->max."</td>
                                                                <td>".$str_sts_active."</td>
                                                                                                                                
                                                                <td>
                                                                <center>
                                                                    ".$btn_sts_active."
                                                                    <button type=\"button\" class=\"btn btn-info\" id=\"up_data\" onclick=\"get_update_data('".$v_list_kriteria->id_kri."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                    <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$v_list_kriteria->id_kri."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                                </td>
                                                            </tr>");
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<?php
$range_min = "";
$range_max = "";
    if(isset($range_hasil)){
        if($range_hasil){
            $range_min = $range_hasil["min"];
            $range_max = $range_hasil["max"];
        }
    }
?>
<!-- ============================================================== -->
<!-- Insert Range Hasil  -->
<!-- ============================================================== -->
<div id="modal_range_hasil" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Target Penilaian</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Target Minimum</label>
                        <input type="number" name="t_min" id="t_min" step="any" class="form-control form-control-line" value="<?php print_r($range_min);?>">
                        <a id="msg_t_min" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Target Maximum</label>
                        <input type="number" name="t_max" id="t_max" step="any" class="form-control form-control-line" value="<?php print_r($range_max);?>">
                        <a id="msg_t_max" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_add_range_hasil" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Insert Range Hasil  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Insert Kriteria  -->
<!-- ============================================================== -->
<div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Tambah data Kriteria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Kriteria</label>
                        <input type="text" name="kri" id="kri" class="form-control form-control-line">
                        <a id="msg_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group" hidden="">
                        <label>Tipe Kriteria</label>
                        <select name="tipe_kri" id="tipe_kri" class="form-control form-control-line">
                            <option value="0">Cost</option>
                            <option value="1">Benefit</option>
                        </select>
                        <a id="msg_tipe_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Nilai Minimum</label>
                        <input type="number" name="n_min" id="n_min" step="any" class="form-control form-control-line">
                        <a id="msg_n_min" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Nilai Maximum</label>
                        <input type="number" name="n_max" id="n_max" step="any" class="form-control form-control-line">
                        <a id="msg_n_max" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_add_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Insert Kriteria  -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Update Kriteria  -->
<!-- ============================================================== -->
<div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Ubah data Kriteria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Kriteria</label>
                        <input type="text" name="kri" id="_kri" class="form-control form-control-line">
                        <a id="_msg_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group" hidden="">
                        <label>Tipe Kriteria</label>
                        <select name="tipe_kri" id="_tipe_kri" class="form-control form-control-line">
                            <option value="0">Cost</option>
                            <option value="1">Benefit</option>
                        </select>
                        <a id="_msg_tipe_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Nilai Minimum</label>
                        <input type="number" name="n_min" id="_n_min" step="any" class="form-control form-control-line">
                        <a id="_msg_n_min" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Nilai Maximum</label>
                        <input type="number" name="n_max" id="_n_max" step="any" class="form-control form-control-line">
                        <a id="_msg_n_max" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_up_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Update Kriteria  -->
<!-- ============================================================== -->


<script type="text/javascript">
//========================================================================
//--------------------------------Insert_target---------------------------
//========================================================================
    $("#btn_add_range_hasil").click(function(){
        // console.log("ok");
        var data_main =  new FormData();
            data_main.append('min' , $("#t_min").val());
            data_main.append('max' , $("#t_max").val());
                
            $.ajax({
                url: "<?php echo base_url()."master/mainkriteria/insert_target_nilai";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    response_insert_range(res);
                }
            });
    });

    function response_insert_range(res){
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                alert(main_msg.msg);
                window.location.href = "<?php print_r(base_url());?>page/kriteria";
            }else{
                $("#msg_t_min").html(detail_msg.min);   
                $("#msg_t_max").html(detail_msg.max); 
                alert(main_msg.msg);             
            }
        }
//========================================================================
//--------------------------------Insert_target---------------------------
//========================================================================


//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================
    $("#btn_add_data").click(function(){
            var data_main =  new FormData();
            data_main.append('kri'      , $("#kri").val());
            data_main.append('tipe_kri' , $("#tipe_kri").val());
            data_main.append('n_min'    , $("#n_min").val());
            data_main.append('n_max'    , $("#n_max").val());
                                                    
            $.ajax({
                url: "<?php echo base_url()."master/mainkriteria/insert_kri";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                alert(main_msg.msg);
                window.location.href = "<?php print_r(base_url());?>page/kriteria";
            }else{
                $("#msg_kri").html(detail_msg.kri);
                $("#msg_tipe_kri").html(detail_msg.tipe_kri);
                $("#msg_n_min").html(detail_msg.n_min);   
                $("#msg_n_max").html(detail_msg.n_max); 
                alert(main_msg.msg);             
            }
        }
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================

var id_data_glob = "";

//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================
    function clear_from_update(){
        $("#_kri").val("");
        $("#_tipe_kri").val("");
        $("#_n_max").val("");
        $("#_n_min").val("");

        id_admin_glob = "";
    }

    function get_update_data(param){
        clear_from_update();

        var data_main =  new FormData();
        data_main.append('id_kri' , param);
                                        
        $.ajax({
            url: "<?php echo base_url()."master/mainkriteria/get_kri_update";?>",
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                set_val_update(res);
                
            }
        });
    }

    function set_val_update(res){
        var res_pemohon = JSON.parse(res.toString());

        if(res_pemohon.status == true){
            var id_data_chahce = res_pemohon.val_response.id_kri;
            $("#_kri").val(res_pemohon.val_response.ket_kri);
            $("#_tipe_kri").val(res_pemohon.val_response.tipe_kri);
            $("#_n_max").val(res_pemohon.val_response.max);
            $("#_n_min").val(res_pemohon.val_response.min);
            
            id_data_glob = res_pemohon.val_response.id_kri;

            $("#update_data").modal("show");
        }else {
            clear_from_update();
        }
    }
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================

//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================
    $("#btn_up_data").click(function() {
        var confirm_msg = confirm("Apa anda yakin merubah data ini ?");
        if(confirm_msg) {
            var data_main = new FormData();
            data_main.append('id_kri', id_data_glob);

            data_main.append('kri' , $("#_kri").val());
            data_main.append('tipe_kri'     , $("#_tipe_kri").val());
            data_main.append('n_min'   , $("#_n_min").val());
            data_main.append('n_max'   , $("#_n_max").val());

            $.ajax({
                    url: "<?php echo base_url()."master/mainkriteria/update_kri";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        response_update(res);
                    }
            });
        }
        
    });

    function response_update(res) {
        var data_json = JSON.parse(res);

        console.log(data_json);
        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            window.location.href = "<?php print_r(base_url());?>page/kriteria";
        } else {
            $("#_msg_kri").html(detail_msg.kri);
            $("#_msg_tipe_kri").html(detail_msg.tipe_kri);
            $("#_msg_n_min").html(detail_msg.n_min);
            $("#_msg_n_max").html(detail_msg.n_max);
        }
    }
//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================

//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================
        function delete_data(param){
            var confirm_msg = confirm("Jika anda menghapus data ini maka seluruh data yang berhubungan dengan data ini akan hilang, Apa anda yakin menghapus data ini ?");
            if(confirm_msg) {
                var data_main =  new FormData();
                data_main.append('id_kri', param);
                                                    
                $.ajax({
                    url: "<?php echo base_url()."master/mainkriteria/delete_kri";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        console.log(res); 
                        alert("hapus data berhasil !, seluruh data yang berkaitan dengan data ini akan terhapus..");
                        location.href="<?php print_r(base_url());?>page/kriteria";
                    }
                });   
            }
        }
//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================


//========================================================================
//--------------------------------non_active_kri--------------------------
//========================================================================
    function get_non_active_kri(id_kri){
        var data_main =  new FormData();
            data_main.append('id_kri'      , id_kri);
            console.log(id_kri);
                                                    
            $.ajax({
                url: "<?php echo base_url()."master/mainkriteria/main_non_active";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    response_insert(res);
                }
            });
    }
//========================================================================
//--------------------------------non_active_kri--------------------------
//========================================================================


//========================================================================
//--------------------------------active_kri-----------------------------
//========================================================================
    function get_active_kri(id_kri){
        var data_main =  new FormData();
            data_main.append('id_kri'      , id_kri);
                                                    
            $.ajax({
                url: "<?php echo base_url()."master/mainkriteria/main_active";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    response_insert(res);
                }
            });
    }
//========================================================================
//--------------------------------active_kri-----------------------------
//========================================================================

</script>