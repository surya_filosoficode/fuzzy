
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>admin_template/assets/images/favicon.png">
    <title>Fuzzy</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/morrisjs/morris.css" rel="stylesheet">
    <!-- Vector CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>admin_template/horizontal/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>admin_template/horizontal/css/colors/blue.css" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/skins/all.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/js/jquery-3.2.1.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<?php
    // print_r($_SESSION);
    if(isset($_SESSION["admin_lv_1"])){
        $nama = $_SESSION["admin_lv_1"]["nama"];
        $email = $_SESSION["admin_lv_1"]["email"];
        $nip = $_SESSION["admin_lv_1"]["nip"];
        $jabatan = $_SESSION["admin_lv_1"]["jabatan"];
    }
?>

<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand">
                        <img src="<?php print_r(base_url());?>admin_template/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                    </a>                         
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown mega-dropdown"><a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-view-grid"></i>&nbsp;&nbsp;SPK Fuzzy (Seleksi Karyawan)</a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-account"></i>&nbsp;&nbsp;<?= $nama;?></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4><?= $nama;?></h4>
                                                <p class="text-muted"><?= $jabatan;?></p></div>
                                        </div>
                                    </li>
                                    <li><a href="<?php print_r(base_url())?>admin/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>
                        <li><a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url())?>beranda" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Beranda</span></a>
                        </li>
                        <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open"></i><span class="hide-menu">Data Master</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url())?>page/karyawan">Data Karyawan</a></li>
                                <li><a href="<?php print_r(base_url())?>page/kriteria">Data Kriteria</a></li>
                                <!-- <li><a href="<?php print_r(base_url())?>page/sub_kriteria">Data Sub. Kriteria</a></li> -->
                            </ul>
                        </li>
                        <li><a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url())?>page/penilaian_karyawan" aria-expanded="false"><i class="mdi mdi-account-edit"></i><span class="hide-menu">Data Penilaian Karyawan</span></a>
                        </li>
                        <li><a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url())?>page/rules" aria-expanded="false"><i class="mdi mdi-lock-pattern"></i><span class="hide-menu">Aturan Fuzzy</span></a>
                        </li>
                        <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Analisis Perhitungan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url())?>page/analisa" target="_blank">Perhitungan</a></li>
                                <li><a href="<?php print_r(base_url())?>page/hasil_perhitungan">Data Hasil Penilaian</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <?php
                switch ($page) {
                    case 'home'             : include "ext/home_page.php"; 
                        break;
                #=================================================================================================#
                #-------------------------------------------main_dosen--------------------------------------------#
                #=================================================================================================#
                    case 'page_dosen'       : include "page_master/dsn_main.php"; 
                        break;

                #=================================================================================================#
                #-------------------------------------------main_kriteria-----------------------------------------#
                #=================================================================================================#
                    case 'page_kriteria'    : include "page_master/kri_main.php"; 
                        break;

                    case 'page_sub_kriteria': include "page_master/sub_kri_main.php"; 
                        break;

                #=================================================================================================#
                #-------------------------------------------main_penilaian----------------------------------------#
                #=================================================================================================#
                    case 'page_penilaian'   : include "page_penilaian/penilaian_dsn_main.php"; 
                        break;

                    case 'page_analisa'     : include "page_penilaian/perhitungan_page.php"; 
                        break;

                    case 'page_hasil'       : include "page_penilaian/hasil_page.php"; 
                        break;

                    case 'page_rules'       : include "page_penilaian/rules.php"; 
                        break;
                    
                    default: include "ext/home_page.php";
                        break;
                }
            ?>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©2019 Create by folosofi_code & design by themedesigner.in </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?=base_url()?>admin_template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--sparkline JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- chartist chart -->
    <script src="<?=base_url()?>admin_template/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!--morris JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/raphael/raphael-min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/morrisjs/morris.min.js"></script>
    <!-- Chart JS -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/dashboard1.js"></script>

    <!-- Vector map JavaScript -->
    <script src="<?=base_url()?>admin_template/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?=base_url()?>admin_template/horizontal/js/dashboard2.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- icheck -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/icheck.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/icheck.init.js"></script>

    <!-- This is data table -->
    <script src="<?=base_url()?>admin_template/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>

    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>