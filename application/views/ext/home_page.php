            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Beranda</h3>
                </div>
                
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card card-inverse card-info">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        <h1 class="text-white"><i class="ti-user"></i></h1></div>
                                    <div>
                                        <h3 class="card-title">Data Karyawan</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 align-self-center">
                                        <h2 class="font-light text-white"><i class="mdi mdi-page-last"></i>&nbsp;<?php print_r($count_dsn);?>&nbsp;&nbsp;Item</h2>
                                    </div>
                                    <div class="col-6 p-t-10 p-b-20 text-right">
                                        <div class="spark-count" style="height:65px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a class="text-white" href="<?= base_url();?>page/karyawan">Lanjut ke halaman Karyawan...</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card card-inverse card-danger">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        <h1 class="text-white"><i class="ti-light-bulb"></i></h1></div>
                                    <div>
                                        <h3 class="card-title">Data Kriteria</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 align-self-center">
                                        <h2 class="font-light text-white"><i class="mdi mdi-page-last"></i>&nbsp;<?php print_r($count_kri);?>&nbsp;&nbsp;Item</h2>
                                    </div>
                                    <div class="col-6 p-t-10 p-b-20 text-right">
                                        <div class="spark-count" style="height:65px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a class="text-white" href="<?= base_url();?>page/kriteria">Lanjut ke halaman Kriteria...</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        <h1 class="text-white"><i class="ti-cup"></i></h1></div>
                                    <div>
                                        <h3 class="card-title">Data Aturan Fuzzy</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 align-self-center">
                                        <h2 class="font-light text-white"><i class="mdi mdi-page-last"></i>&nbsp;<?php print_r($count_rules);?>&nbsp;&nbsp;Item</h2>
                                    </div>
                                    <div class="col-6 p-t-10 p-b-20 text-right">
                                        <div class="spark-count" style="height:65px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a class="text-white" href="<?= base_url();?>page/rules">Lanjut ke halaman Aturan Fuzzy...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->

                <!-- Row -->
                <!-- <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <div class="col-lg-12 text-right">
                                        <button type="button" class="btn btn-info btn-rounded" id="btn_go_hasil">
                                                <i class="fa fa-plus-circle"></i> Lihat Perhitungan
                                        </button>
                                    </div>
                                </div>
                                <div class="table-responsive m-t-20">
                                    <table class="table nowrap stylish-table">
                                        <thead>
                                            <tr>
                                                <th>Id karyawan</th>
                                                <th>Nama karyawan</th>
                                                <th>NIDN</th>
                                                <th>Hasil Penilaian</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                                if($hasil){
                                                    foreach ($hasil as $key => $value) {
                                                        print_r("<tr>
                                                                    <td>".$value->id_dsn."</td>
                                                                    <td>".$value->nama."</td>
                                                                    <td>".$value->nidn."</td>
                                                                    <td align=\"right\">".$value->hasil."</td>
                                                                </tr>");
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- Row -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script type="text/javascript">
    $("#btn_go_hasil").click(function(){
        window.location.href = "<?= base_url();?>page/hasil_perhitungan";
    });
</script>