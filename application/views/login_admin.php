
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>admin_template/assets/images/favicon.png">
    <title>Fuzzy</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>admin_template/horizontal/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>admin_template/horizontal/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image: linear-gradient(to bottom right, #bad2de, #68add0);">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform" method="post" action="<?php print_r(base_url())?>admin/login/auth">
                        <center><h3 class="box-title m-b-20">Halaman Login Admin</h3></center><br><br>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" id="email" name="email" required="" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" id="password" name="password" required="" placeholder="Password">
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group text-center m-t-50">
                            <div class="col-xs-12">
                                <button class="btn btn-rounded btn-info" type="submit" style="width: 150px;">
                                    <i class="mdi mdi-login-variant"></i> &nbsp;&nbsp;Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>