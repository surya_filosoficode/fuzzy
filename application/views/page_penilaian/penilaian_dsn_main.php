            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <?php
                // print_r($_SESSION)
            ?>
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Halaman Penilaian Karyawan</h3>
                </div>
                
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4 class="card-title">List Data Penilaian Karyawan</h4>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data">
                                            <i class="fa fa-plus-circle"></i> Tambah Penilaian Karyawan
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>No. Karyawan</th>
                                                <th>Nama Karyawan</th>
                                                <th>Verifikasi Penilaian</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if($penilainan_all){
                                                    $no = 1;
                                                    $count_kriteria = count($kriteria);
                                                    foreach ($penilainan_all as $r_penilainan_all => $v_penilainan_all) {
                                                        $count_penilaian = count($v_penilainan_all["val"]);

                                                        $status = "Tidak ternilai dengan sempurna";
                                                        if($count_penilaian == $count_kriteria){
                                                            $status = "Ternilai dengan sempurna";
                                                            print_r("<tr>
                                                                    <td>".($no++)."</td>
                                                                    <td>".$v_penilainan_all["dsn"]->nidn."</td>
                                                                    <td>".$v_penilainan_all["dsn"]->nama."</td>
                                                                    <td>".$status."</td>
                                                                    <td>
                                                                    <center>
                                                                        <button type=\"button\" class=\"btn btn-info\" id=\"up_data\" onclick=\"get_update_data('".$v_penilainan_all["dsn"]->id_dsn."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                        <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$v_penilainan_all["dsn"]->id_dsn."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                    </center>
                                                                    </td>
                                                                </tr>
                                                                ");
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Insert Penilaian Karyawan  -->
<!-- ============================================================== -->
<div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Penilaian Karyawan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="<?php print_r(base_url())?>topsis/mainpenilaian/insert_penilaian_dsn">
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Karyawan</label>
                            <select class="form-control" id="dsn" name="dsn">
                                <?php
                                // print_r($dsn_ins);
                                    if($dsn_ins){
                                        foreach ($dsn_ins as $v_dsn) {
                                            echo "<option id=\"".$v_dsn->id_dsn."\" value=\"".$v_dsn->id_dsn."\">".$v_dsn->nidn." - ".$v_dsn->nama."</option>";
                                        }
                                    }
                                ?> 
                            </select>
                        <a id="msg_nama_dsn" style="color: red;"></a>
                    </div>
                    <?php
                        if(!empty($kriteria)){
                            foreach ($kriteria as $val_kriteria) {
                    ?>
                            <div class="form-group">
                                <label><?= $val_kriteria->ket_kri;?> (min = <?= $val_kriteria->min;?>, max = <?= $val_kriteria->max;?>)*</label>
                                <input class="form-control" min="<?= $val_kriteria->min;?>" max="<?= $val_kriteria->max;?>" id="kri_<?= $val_kriteria->id_kri;?>" name="kri_<?= $val_kriteria->id_kri;?>">
                            </div>
                    <?php
                            }
                        }

                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_add_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Insert Penilaian Karyawan  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Update Penilaian Karyawan  -->
<!-- ============================================================== -->
<div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Ubah Penilaian Karyawan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form method="post" action="<?php print_r(base_url())?>topsis/mainpenilaian/update_penilaian_dsn"> -->
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Karyawan</label>
                            <select class="form-control" id="_dsn" name="dsn" disabled="">
                                <?php
                                    if($dsn){
                                        foreach ($dsn as $v_dsn) {
                                            echo "<option id=\"".$v_dsn->id_dsn."\" value=\"".$v_dsn->id_dsn."\">".$v_dsn->nidn." - ".$v_dsn->nama."</option>";
                                        }
                                    }
                                ?> 
                            </select>
                        <a id="_msg_nama_dsn" style="color: red;"></a>
                    </div>
                    <?php
                        if(!empty($kriteria)){
                            foreach ($kriteria as $val_kriteria) {
                    ?>
                            <div class="form-group">
                                <label><?= $val_kriteria->ket_kri;?> (min = <?= $val_kriteria->min;?>, max = <?= $val_kriteria->max;?>)*</label>
                                <input class="form-control" min="<?= $val_kriteria->min;?>" max="<?= $val_kriteria->max;?>" id="_kri_<?= $val_kriteria->id_kri;?>" name="kri_<?= $val_kriteria->id_kri;?>">
                                    <!-- <select class="form-control" id="_kri_<?= $val_kriteria->id_kri;?>" name="kri_<?= $val_kriteria->id_kri;?>">
                                            <?php
                                                if(!empty($val_kriteria->id_kri)){
                                                    foreach ($sub_kri[$val_kriteria->id_kri] as $val_sub_kri) {
                                                        echo "<option id=\"".$val_sub_kri->id_sub_kri."\" value=\"".$val_sub_kri->id_sub_kri."\">".$val_sub_kri->ket_sub."</option>";
                                                    }
                                                }
                                            ?>             
                                    </select> -->
                            </div>
                    <?php
                            }
                        }

                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_up_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Update Penilaian Karyawan  -->
<!-- ============================================================== -->

<script type="text/javascript">


var id_data_glob = "";
    
    $(document).ready(function(){
        <?php
            if(isset($_SESSION["response_send"]))
            print_r("alert('".$_SESSION["response_send"]["msg_main"]["msg"]."');")
        ?>
    });
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================
    function clear_from_update(){
        $("#_dsn").val(""); 

        id_admin_glob = "";
    }

    function get_update_data(param){
        clear_from_update();

        var data_main =  new FormData();
        data_main.append('nidn' , param);
                                        
        $.ajax({
            url: "<?php echo base_url()."topsis/Mainpenilaian/get_penilaian_update";?>",
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                set_val_update(res);
                
            }
        });
    }

    function set_val_update(res){
        var res_pemohon = JSON.parse(res.toString());
        var count_kriteria = <?=$count_kriteria?>;
        console.log(res_pemohon);
        if(res_pemohon.status == true){
            for (var i = 0; i < res_pemohon["val_response"].length; i++) {
                var id_kri = "#_kri_"+res_pemohon["val_response"][i].id_kri;
                // console.log(res_pemohon["val_response"][i].id_sub_kri);

                $(id_kri).val(res_pemohon["val_response"][i].penilaian);

                var nidn = res_pemohon["val_response"][i].nidn;
            }
            id_data_glob = nidn;
            $("#_dsn").val(id_data_glob);
        }else {
            clear_from_update();
        }

        $("#update_data").modal("show");
    }
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================

//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================
    $("#btn_up_data").click(function() {
        $("#_dsn").removeAttr("disabled", true);
        var confirm_msg = confirm("Apa anda yakin merubah data ini ?");
        if(confirm_msg) {
            var data_main = new FormData();
            data_main.append("dsn", $("#_dsn").val());

            <?php
                foreach ($kriteria as $r_kriteria => $v_kriteria) {
                    print_r("data_main.append('kri_".$v_kriteria->id_kri."', $(\"#_kri_".$v_kriteria->id_kri."\").val());\n");
                }
            ?>
                                    
            $.ajax({
                    url: "<?php echo base_url()."topsis/mainpenilaian/update_penilaian_dsn";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        response_update(res);
                    }
            });
        }

        $("#_dsn").attr("disabled", true);
        
    });

    function response_update(res) {
        var data_json = JSON.parse(res);

        console.log(data_json);
        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            alert(main_msg.msg);
            window.location.href = "<?php print_r(base_url());?>page/penilaian_karyawan";
        } else {
            // $("#_msg_kri").html(detail_msg.kri);
            // $("#_msg_tipe_kri").html(detail_msg.tipe_kri);
            // $("#_msg_bobot").html(detail_msg.bobot);
        }
    }
//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================

//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================
        function delete_data(param){
            var confirm_msg = confirm("Jika anda menghapus data ini maka seluruh data yang berhubungan dengan data ini akan hilang, Apa anda yakin menghapus data ini ?");
            if(confirm_msg) {
                var data_main =  new FormData();
                data_main.append('dsn', param);
                                                    
                $.ajax({
                    url: "<?php echo base_url()."topsis/mainpenilaian/delete_penilaian_dsn";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        console.log(res); 
                        alert("hapus data berhasil !, seluruh data yang berkaitan dengan data ini akan terhapus..");
                        location.href="<?php print_r(base_url());?>page/penilaian_karyawan";
                    }
                });   
            }
        }
//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================

</script>