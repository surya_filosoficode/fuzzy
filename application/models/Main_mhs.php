<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_mhs extends CI_Model {


#------------------------------------------------------------------Mahasiswa----------------------------------------------------------------------

	public function mhs_insert($data){
		$insert = $this->db->insert("mhs", $data);
		return $insert;
	}

	public function mhs_update($set, $where){
		$update = $this->db->update("mhs",$set, $where);
		return $update;
	}

	public function mhs_delete($where){
		$delete = $this->db->delete("mhs", $where);
		return $delete;
	}

	public function mhs_get(){
		$data = $this->db->get("mhs")->result();
		return $data;
	}

	public function mhs_get_where($where){
		$data = $this->db->get_where("mhs", $where)->row_array();
		return $data;
	}

	public function mhs_like_mhs($id){
		$data = $this->db->query("select * from mhs where nim like '%".$id."%'")->result();
		return $data;
	}

	
}
