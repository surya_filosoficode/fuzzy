<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_sk extends CI_Model {

#------------------------------------------------------------------Mahasiswa----------------------------------------------------------------------

	public function sk_insert($data){
		$insert = $this->db->insert("skripsi", $data);
		return $insert;
	}

	public function sk_update($set, $where){
		$update = $this->db->update("skripsi",$set, $where);
		return $update;
	}

	public function sk_delete($where){
		$delete = $this->db->delete("skripsi", $where);
		return $delete;
	}

	public function sk_get(){
		$this->db->join("mhs m", "sk.nim=m.nim");
		$data = $this->db->get("skripsi sk")->result();
		return $data;
	}

	public function sk_get_where($where){
		$data = $this->db->get_where("skripsi", $where)->row_array();
		return $data;
	}


}
