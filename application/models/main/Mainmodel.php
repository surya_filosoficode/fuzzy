<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmodel extends CI_Model{
    
    public function get_data_all($table){
    	$data = $this->db->get($table);
    	return $data->result();
    }

    public function get_data_all_where($table, $where){
    	$data = $this->db->get_where($table, $where);
    	return $data->result();
    }

    public function get_data_all_distict($table, $by){
        $this->db->select($by);
        $this->db->distinct();
        $data = $this->db->get($table);
        return $data->result();
    }

    public function get_data_each($table, $where){
    	$data = $this->db->get_where($table, $where);
    	return $data->row_array();
    }

    public function insert_data($table, $data){
    	$insert = $this->db->insert($table, $data);
    	return $insert;
    }

    public function update_data($table, $set, $where){
    	$update = $this->db->update($table, $set, $where);
    	return $update;
    }

    public function delete_data($table, $where){
    	$delete = $this->db->delete($table, $where);
    	return $delete;
    }

    public function get_full_penilaian($where){
        $this->db->join("kriteria k", "p.id_kri=k.id_kri");
        // $this->db->join("kriteria_sub ks", "p.id_sub_kri=ks.id_sub_kri");
        return $this->db->get_where("penilaian p", $where)->result();
    }

    public function get_hasil(){
        $this->db->select("d.id_dsn, d.nama, d.nidn, hasil, CAST(hasil as DECIMAL(9,3)) _hasil");
        $this->db->join("dsn d", "d.id_dsn=h.nidn");
        $this->db->order_by("_hasil", "desc");
        return $this->db->get("dummy_hasil h")->result();
    }

}
?>