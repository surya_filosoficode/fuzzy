<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Model{
    
    public function get_admin($where){
        $admin = $this->db->get_where("admin", $where);
        return $admin;
    }    
}
?>