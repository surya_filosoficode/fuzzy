<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_ahp extends CI_Model {

	public function get_kri(){
		$data = $this->db->get("kriteria")->result();
		return $data;
	}

	public function get_where_kri($where){
		$data = $this->db->get_where("kriteria", $where)->result();
		return $data;
	}

	public function get_perbandingan($where){
		$data = $this->db->get_where("perbandingan_kri", $where)->result();
		return $data;
	}

	public function get_ri(){
		$data = $this->db->get("ri")->result();
		$data_r = array();
		foreach ($data as $val_data) {
			$data_r[$val_data->n] = $val_data->val_ri;
		}

		return $data_r;
	}

	public function perbandingan_insert($data){
		$insert = $this->db->insert("perbandingan_kri", $data);
		return $insert;
	}

	public function perbandingan_each_kri($where){
		$this->db->or_where("col_per", $where["id_kri"]);
		$this->db->or_where("row_per", $where["id_kri"]);
		$delete = $this->db->delete("perbandingan_kri");
		return $delete;
	}

	
}
