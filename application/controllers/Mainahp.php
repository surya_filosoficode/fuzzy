<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainahp extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("Main_ahp", "ma");
		$this->load->model("main_dsn", "md");

        $this->load->library("response_message");

        if(isset($_SESSION["admin_lv_1"])){
			if($this->session->userdata("admin_lv_1")["is_log"] != 1){
				redirect(base_url()."admin/login");
			}
		}else{
			redirect(base_url()."admin/login");
		}
        
	}

	public function index(){
		$data["page"] = "main_ahp";

		$kri = $this->ma->get_kri();
		$data["kri"] = $kri;
		$ri = $this->ma->get_ri();
		$data["ri"] = $ri;
		print_r("<pre>");

		#------------- set basic var -------------

		$count_kri = count($kri);
		$count_col_per = array();
		
		$list_id_col = array();
		$list_val_col = array();

		$list_id_row = array();
		$list_val_row = array();

		$list_id_kri = array();
		$list_tipe_kri = array();
		#------------- set basic var -------------

		#------------- set perbandingan var -------------


		foreach ($kri as $r_kri => $val_kri) {
			// print_r($val_kri);
			$data_perbandingan_col = $this->ma->get_perbandingan(array("col_per"=>$val_kri->id_kri));
			// print_r($da1ta_perbandingan_col);
			$temp_col = array();
			$temp_id_col = array();

			$list_id_kri[$r_kri] = $val_kri->ket_kri;
			foreach ($data_perbandingan_col as $val_col) {
				array_push($temp_col, $val_col->val_per);
				array_push($temp_id_col, $val_col->id_per);
			}

			// array_push($list_val_col, $temp_col);
			// array_push($list_id_col, $temp_id_col);

			$list_val_col[$val_kri->id_kri] = $temp_col;
			$list_id_col[$val_kri->id_kri] = $temp_id_col;

			$data_perbandingan_row = $this->ma->get_perbandingan(array("row_per"=>$val_kri->id_kri));
			// print_r($da1ta_perbandingan_col);
			$temp_row = array();
			$temp_id_row = array();
			foreach ($data_perbandingan_row as $val_row) {
				array_push($temp_row, $val_row->val_per);
				array_push($temp_id_row, $val_row->id_per);
			}

			$list_val_row[$val_kri->id_kri] = $temp_row;
			$list_id_row[$val_kri->id_kri] = $temp_id_row;
			
			// $count_col_per[$val_kri->id_kri] = array_sum($temp_col);
			array_push($count_col_per, array_sum($temp_col));
			array_push($list_tipe_kri, $val_kri->tipe_kri);
			// print_r("--------------kriteria------------<br>");
			// print_r($val_kri->id_kri);
			// print_r($count_col_per);
			// print_r("--------------kriteria------------<br><br>");
						// array_push($list_val_row, $temp_row);
			// array_push($list_id_row, $temp_id_row);
		}

		#------------- set perbandingan var -------------
		$data["list_tipe_kri"] = $list_tipe_kri;
		// print_r($list_id_col);
		// print_r($list_val_col);
		// echo "#------------- Kriteria -------------<br>";
		// print_r($list_id_kri);
		$data["list_id_kri"] = $list_id_kri;
		// print_r($list_id_row);
		// echo "#------------- set perbandingan var -------------<br>";
		// print_r($list_val_row);
		$data["set_perbandingan_awal"] = $list_val_row;
		// print_r($count_col_per);
		// $this->load->view('main_index',$data);

		#------------- set priority var -------------
		$list_new_matrix = array();
		$list_priority = array();
		// $list_sum_row = array();
		// $priority_row_tmp = array();
		foreach ($kri as $r_kri =>$val_kri) {
			$sum_row_temp = 0;
			foreach ($list_val_row[$val_kri->id_kri] as $r_list_row => $val_list_row) {
				$val_rata_perbandingan = number_format($val_list_row / $count_col_per[$r_list_row], 4, ".","");
				$list_new_matrix[$r_kri][$r_list_row] = number_format($val_list_row / $count_col_per[$r_list_row], 4, ".","");
				// print_r($count_col_per[$r_list_row]);
				// print_r(" / ");
				// print_r($val_list_row);
				// print_r(" --- ");
				// print_r($val_rata_perbandingan);
				// print_r(" | ");
				// array_push($priority_row_tmp, $val_rata_perbandingan);
				$sum_row_temp += $val_rata_perbandingan;
			}

			// $list_priority[$val_kri->id_kri] = $sum_row_temp/$count_kri;
			array_push($list_priority, $sum_row_temp/$count_kri);
			// print_r(" <---> ");
			// print_r($sum_row_temp/$count_kri);
			// echo "<br>";
		}
		// echo "#------------- set new matrix -------------<br>";
		// print_r($list_new_matrix);
		$data["set_new_matrix"] = $list_new_matrix;
		// echo "#------------- set priority var -------------<br>";
		// print_r($list_priority);
		$data["set_priority"] = $list_priority;

		#------------- set priority var -------------


		#------------- set kriteria vs rata-rata var -------------

		$list_kri_vs_rata = array();
		foreach ($kri as $val_kri) {
			$count_data_n = 0;
			foreach ($list_val_row[$val_kri->id_kri] as $r_list_row => $val_list_row) {
				$data_n = $val_list_row * $list_priority[$r_list_row];
				$count_data_n += $data_n;
				
			}
			// print_r($count_data_n);
			// print_r(" --- ");
			array_push($list_kri_vs_rata, $count_data_n);
		}
		// print_r("#------------- set kriteria vs rata-rata var -------------<br>");
		// print_r($list_kri_vs_rata);
		$data["list_kri_vs_rata"] = $list_kri_vs_rata;

		#------------- set kriteria vs rata-rata var -------------



		#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------
			$count_kri_vs_rata = 0;
			foreach ($list_kri_vs_rata as $r_kri_vs_rata =>$val_kri_vs_rata) {
				$count_kri_vs_rata += $val_kri_vs_rata/$list_priority[$r_kri_vs_rata];
			}

			$count_kri_vs_rata = $count_kri_vs_rata/$count_kri;
			$data["count_kri_vs_rata"]=$count_kri_vs_rata;
			// print_r("#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------<br>");
			// print_r($count_kri_vs_rata);
			// echo "<br>";
		#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------

		#------------- Indeks Consistensi -------------
			$konsistensi = ($count_kri_vs_rata - $count_kri)/($count_kri-1);
			$data["konsistensi"]=$konsistensi;
			// print_r("#------------- Indeks Consistensi -------------<br>");
			// print_r($konsistensi);
			// echo "<br>";
		#------------- Indeks Consistensi -------------
												
		#------------- Indeks Consistensi / ri -------------
			$konsistensi_ri = $konsistensi / $ri[$count_kri];
			$data["konsistensi_ri"]=$konsistensi_ri;
			// print_r("#------------- Indeks Consistensi / ri -------------<br>");
			// print_r($konsistensi_ri);
			// echo "<br>";


		#------------- Indeks Consistensi -------------

			if($konsistensi_ri <= 0.1){
#------------- Kriteria Dosen -------------
			$dsn = $this->md->dsn_get();
			$count_dsn = count($dsn);

			$id_r_dsn = array();
			$matrix_penilaian_dsn = array();

			$penilainan_all = array();
			foreach ($dsn as $r_dsn => $val_dsn) {
				array_push($id_r_dsn, $val_dsn->nidn);
				$penilaian_dsn= $this->md->get_all_penilaian(array("nidn"=>$val_dsn->nidn))->result();
				foreach ($penilaian_dsn as $r_penilaian_dsn => $val_penilaian_dsn) {
					$matrix_penilaian_dsn[$r_penilaian_dsn][$r_dsn] = $val_penilaian_dsn->val_sub;

				}

				// print_r($penilaian_dsn);
				// print_r("----------------------------------------------------------");
			}

			// print_r("#----------------------------Dosen------------------------------<br>");
			// print_r($id_r_dsn);
			$data["id_r_dsn"] = $id_r_dsn;
			
			// print_r("#----------------------------Tipe Kriteria------------------------------<br>");
			// print_r($list_tipe_kri);

			// print_r("#----------------------------Kriteria Dosen------------------------------<br>");
			// print_r($matrix_penilaian_dsn);
			$data["matrix_penilaian_dsn"] = $matrix_penilaian_dsn;
			
		#------------- Kriteria Dosen -------------		

		#------------- Perbandingan Kriteria Dosen -------------
			$list_per_dsn = array();
			$count_list_kri_dsn = array();
			foreach ($matrix_penilaian_dsn as $r_matrix_penilaian_dsn => $val_matrix_penilaian_dsn) {
				$count_list_kri_dsn[$r_matrix_penilaian_dsn] = array();
				foreach ($val_matrix_penilaian_dsn as $r_row_matrix_penilaian_dsn => $v_row_matrix_penilaian_dsn) {
					foreach ($val_matrix_penilaian_dsn as $r_col_matrix_penilaian_dsn => $v_col_matrix_penilaian_dsn) {

						$temp_list_per_dsn = 0;
						if($list_tipe_kri[$r_matrix_penilaian_dsn] == 1){
							// $list_per_dsn[$r_matrix_penilaian_dsn][$r_row_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] 
							$temp_list_per_dsn = $v_col_matrix_penilaian_dsn/$v_row_matrix_penilaian_dsn;
						}else {
							$temp_list_per_dsn = $v_row_matrix_penilaian_dsn/$v_col_matrix_penilaian_dsn;
						}
						
						$list_per_dsn[$r_matrix_penilaian_dsn][$r_row_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] = $temp_list_per_dsn;

						if(array_key_exists($r_col_matrix_penilaian_dsn, $count_list_kri_dsn[$r_matrix_penilaian_dsn])){
							$count_list_kri_dsn[$r_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] += $temp_list_per_dsn;

							// $v_col_matrix_penilaian_dsn/$v_row_matrix_penilaian_dsn;
							// echo "ada<br>";
						}else {
							// echo "gak ada<br>";
							$count_list_kri_dsn[$r_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] = $temp_list_per_dsn;

							// $v_col_matrix_penilaian_dsn/$v_row_matrix_penilaian_dsn;
						}	
					}
				}
			}
			// print_r("#----------------------------Perbandingan Kriteria Dosen------------------------------<br>");
			// print_r($list_per_dsn);
			$data["list_per_dsn"] = $list_per_dsn; 

			// print_r("#----------------------------Total Perbandingan Kriteria Dosen------------------------------<br>");
			// print_r($count_list_kri_dsn);
			$data["count_list_kri_dsn"] = $count_list_kri_dsn; 

			
		#------------- Perbandingan Kriteria Dosen -------------		

		#------------- Perhitungan Perbandingan Antar Alternatif -------------

			// print_r("#----------------------------Perhitungan Perbandingan Antar Alternatif------------------------------<br>");
			
			$list_val_perhitungan_matrix = array();
			$list_count_perhitungan_matrix = array();
			// $list_count_perhitungan_matrix_vs_prior = array();
			foreach ($list_per_dsn as $r_per_kri => $v_per_kri) {
				// $list_tmp_perhitungan_matrix_vs_prior = 0;
				foreach ($v_per_kri as $r_row_per_kri => $v_row_per_kri) {
					$temp_count_per_kri = 0;
					foreach ($v_row_per_kri as $r_col_per_kri => $v_col_per_kri) {
						$temp_per_kri = $v_col_per_kri / $count_list_kri_dsn[$r_per_kri][$r_col_per_kri];
						
						$list_val_perhitungan_matrix[$r_per_kri][$r_row_per_kri][$r_col_per_kri] = $temp_per_kri;

						// echo  $temp_per_kri. " | ";
						$temp_count_per_kri += $temp_per_kri; 
						// print_r();
					}

					$list_count_perhitungan_matrix[$r_row_per_kri][$r_per_kri] = $temp_count_per_kri/$count_dsn ;
					// echo $temp_count_per_kri/$count_dsn;
					// echo "<br>";
					// $list_tmp_perhitungan_matrix_vs_prior += ($temp_count_per_kri/$count_dsn) * $list_priority[$r_row_per_kri];
				}
				// echo $list_tmp_perhitungan_matrix_vs_prior;
				// echo "<br>";
				// $list_count_perhitungan_matrix_vs_prior[$r_per_kri] = $list_tmp_perhitungan_matrix_vs_prior;
				// echo "<br><hr><br>";
			}

			// print_r("#----------------------------Perhitungan Perbandingan Antar Alternatif------------------------------<br>");
			// print_r($list_val_perhitungan_matrix);
			$data["list_val_perhitungan_matrix"] = $list_val_perhitungan_matrix;
			// print_r("#----------------------------Total Row Perbandingan Antar Alternatif------------------------------<br>");
			// print_r($list_count_perhitungan_matrix);
			$data["list_count_perhitungan_matrix"] = $list_count_perhitungan_matrix;
			// print_r("#-------------------------------Table Priority---------------------------<br>");
			// print_r($list_priority);


			// print_r("----------------------------------------------------------<br>");
			// print_r($list_count_perhitungan_matrix_vs_prior);

		#------------- Perhitungan Perbandingan Antar Alternatif -------------

		
		#------------- Perhitungan Akhir : -------------
			$perhitungan_hasil = array();
			foreach ($list_count_perhitungan_matrix as $r_list_count_perhitungan_matrix => $v_list_count_perhitungan_matrix) {
				$tmp_col_list_count_perhitungan_matrix = 0;
				foreach ($v_list_count_perhitungan_matrix as $r_col_list_count_perhitungan_matrix => $v_col_list_count_perhitungan_matrix) {
					$tmp_col_list_count_perhitungan_matrix += $v_col_list_count_perhitungan_matrix*$list_priority[$r_col_list_count_perhitungan_matrix];
				}
				$perhitungan_hasil[$r_list_count_perhitungan_matrix] = number_format($tmp_col_list_count_perhitungan_matrix, 5, '.','');
			}

			// print_r("#-------------------- Perhitungan Akhir : ------------------<br>");
			// print_r($perhitungan_hasil);
			// $sort = asort($perhitungan_hasil, SORT_NUMERIC);
			// print_r($sort);
			$data["perhitungan_hasil"] = $perhitungan_hasil;
			// print_r("----------------------------------------------------------<br>");

		#------------- Perhitungan Akhir : -------------

			}else {
				echo "tidak konsisten";
			}

			// print_r($list_tipe_kri);
			// print_r($data);
			$this->load->view("ahp/main_hasil", $data);
	}


	public function process(){
		// $data["page"] = "main_ahp";

		#------------- set basic response -------------

		$response = array("status" => false, "data" => null);

		#------------- set basic response -------------

		$kri = $this->ma->get_kri();
		$data["kri"] = $kri;
		$ri = $this->ma->get_ri();
		$data["ri"] = $ri;
		// print_r("<pre>");

		#------------- set basic var -------------

		$count_kri = count($kri);
		$count_col_per = array();
		
		$list_id_col = array();
		$list_val_col = array();

		$list_id_row = array();
		$list_val_row = array();

		$list_id_kri = array();
		$list_tipe_kri = array();
		#------------- set basic var -------------

		#------------- set perbandingan var -------------


		foreach ($kri as $r_kri => $val_kri) {
			// print_r($val_kri);
			$data_perbandingan_col = $this->ma->get_perbandingan(array("col_per"=>$val_kri->id_kri));
			// print_r($da1ta_perbandingan_col);
			$temp_col = array();
			$temp_id_col = array();

			$list_id_kri[$r_kri] = $val_kri->ket_kri;
			foreach ($data_perbandingan_col as $val_col) {
				array_push($temp_col, $val_col->val_per);
				array_push($temp_id_col, $val_col->id_per);
			}

			

			$list_val_col[$val_kri->id_kri] = $temp_col;
			$list_id_col[$val_kri->id_kri] = $temp_id_col;

			$data_perbandingan_row = $this->ma->get_perbandingan(array("row_per"=>$val_kri->id_kri));
			
			$temp_row = array();
			$temp_id_row = array();
			foreach ($data_perbandingan_row as $val_row) {
				array_push($temp_row, $val_row->val_per);
				array_push($temp_id_row, $val_row->id_per);
			}

			$list_val_row[$val_kri->id_kri] = $temp_row;
			$list_id_row[$val_kri->id_kri] = $temp_id_row;
			
			
			array_push($count_col_per, array_sum($temp_col));
			array_push($list_tipe_kri, $val_kri->tipe_kri);
			
		}

		#------------- set perbandingan var -------------
		// $data["list_tipe_kri"] = $list_tipe_kri;
		
		// echo "#------------- Kriteria -------------<br>";
		
		// $data["list_id_kri"] = $list_id_kri;
		
		// echo "#------------- set perbandingan var -------------<br>";
		
		// $data["set_perbandingan_awal"] = $list_val_row;
		

		#------------- set priority var -------------
		$list_new_matrix = array();
		$list_priority = array();
		
		foreach ($kri as $r_kri =>$val_kri) {
			$sum_row_temp = 0;
			foreach ($list_val_row[$val_kri->id_kri] as $r_list_row => $val_list_row) {
				$val_rata_perbandingan = number_format($val_list_row / $count_col_per[$r_list_row], 4, ".","");
				$list_new_matrix[$r_kri][$r_list_row] = number_format($val_list_row / $count_col_per[$r_list_row], 4, ".","");
				
				$sum_row_temp += $val_rata_perbandingan;
			}

			array_push($list_priority, $sum_row_temp/$count_kri);
			
		}
		// echo "#------------- set new matrix -------------<br>";
		
		// $data["set_new_matrix"] = $list_new_matrix;
		// echo "#------------- set priority var -------------<br>";
		
		// $data["set_priority"] = $list_priority;

		#------------- set priority var -------------


		#------------- set kriteria vs rata-rata var -------------

		$list_kri_vs_rata = array();
		foreach ($kri as $val_kri) {
			$count_data_n = 0;
			foreach ($list_val_row[$val_kri->id_kri] as $r_list_row => $val_list_row) {
				$data_n = $val_list_row * $list_priority[$r_list_row];
				$count_data_n += $data_n;
				
			}
			
			array_push($list_kri_vs_rata, $count_data_n);
		}
		
		// $data["list_kri_vs_rata"] = $list_kri_vs_rata;

		#------------- set kriteria vs rata-rata var -------------



		#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------
			$count_kri_vs_rata = 0;
			foreach ($list_kri_vs_rata as $r_kri_vs_rata =>$val_kri_vs_rata) {
				$count_kri_vs_rata += $val_kri_vs_rata/$list_priority[$r_kri_vs_rata];
			}

			$count_kri_vs_rata = $count_kri_vs_rata/$count_kri;
			// $data["count_kri_vs_rata"]=$count_kri_vs_rata;
			
		#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------

		#------------- Indeks Consistensi -------------
			$konsistensi = ($count_kri_vs_rata - $count_kri)/($count_kri-1);
			// $data["konsistensi"]=$konsistensi;
			
		#------------- Indeks Consistensi -------------
												
		#------------- Indeks Consistensi / ri -------------
			$konsistensi_ri = $konsistensi / $ri[$count_kri];
			// $data["konsistensi_ri"]=$konsistensi_ri;
			
		#------------- Indeks Consistensi -------------

			if($konsistensi_ri <= 0.1){
#------------- Kriteria Dosen -------------
			$dsn = $this->md->dsn_get();
			$count_dsn = count($dsn);

			$id_r_dsn = array();
			$matrix_penilaian_dsn = array();

			$data_dsn = array();

			$penilainan_all = array();
			foreach ($dsn as $r_dsn => $val_dsn) {
				array_push($id_r_dsn, $val_dsn->nidn);
				$data_dsn[$val_dsn->nidn] = $val_dsn;
				$penilaian_dsn= $this->md->get_all_penilaian(array("nidn"=>$val_dsn->nidn))->result();
				foreach ($penilaian_dsn as $r_penilaian_dsn => $val_penilaian_dsn) {
					$matrix_penilaian_dsn[$r_penilaian_dsn][$r_dsn] = $val_penilaian_dsn->val_sub;

				}

				// print_r($penilaian_dsn);
				// print_r("----------------------------------------------------------");
			}

			// print_r("#----------------------------Dosen------------------------------<br>");
			// print_r($id_r_dsn);
			// $data["id_r_dsn"] = $id_r_dsn;
			$data["data_dsn"] = $data_dsn;
			// print_r("#----------------------------Tipe Kriteria------------------------------<br>");
			// print_r($list_tipe_kri);

			// print_r("#----------------------------Kriteria Dosen------------------------------<br>");
			// print_r($matrix_penilaian_dsn);
			// $data["matrix_penilaian_dsn"] = $matrix_penilaian_dsn;
			
		#------------- Kriteria Dosen -------------		

		#------------- Perbandingan Kriteria Dosen -------------
			$list_per_dsn = array();
			$count_list_kri_dsn = array();
			foreach ($matrix_penilaian_dsn as $r_matrix_penilaian_dsn => $val_matrix_penilaian_dsn) {
				$count_list_kri_dsn[$r_matrix_penilaian_dsn] = array();
				foreach ($val_matrix_penilaian_dsn as $r_row_matrix_penilaian_dsn => $v_row_matrix_penilaian_dsn) {
					foreach ($val_matrix_penilaian_dsn as $r_col_matrix_penilaian_dsn => $v_col_matrix_penilaian_dsn) {

						$temp_list_per_dsn = 0;
						if($list_tipe_kri[$r_matrix_penilaian_dsn] == 1){
							$temp_list_per_dsn = $v_col_matrix_penilaian_dsn/$v_row_matrix_penilaian_dsn;
						}else {
							$temp_list_per_dsn = $v_row_matrix_penilaian_dsn/$v_col_matrix_penilaian_dsn;
						}
						
						$list_per_dsn[$r_matrix_penilaian_dsn][$r_row_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] = $temp_list_per_dsn;

						if(array_key_exists($r_col_matrix_penilaian_dsn, $count_list_kri_dsn[$r_matrix_penilaian_dsn])){
							$count_list_kri_dsn[$r_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] += $temp_list_per_dsn;

						}else {
							$count_list_kri_dsn[$r_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] = $temp_list_per_dsn;

						}	
					}
				}
			}
			// print_r("#----------------------------Perbandingan Kriteria Dosen------------------------------<br>");
			// print_r($list_per_dsn);
			// $data["list_per_dsn"] = $list_per_dsn; 

			// print_r("#----------------------------Total Perbandingan Kriteria Dosen------------------------------<br>");
			// print_r($count_list_kri_dsn);
			// $data["count_list_kri_dsn"] = $count_list_kri_dsn; 

			
		#------------- Perbandingan Kriteria Dosen -------------		

		#------------- Perhitungan Perbandingan Antar Alternatif -------------

			// print_r("#----------------------------Perhitungan Perbandingan Antar Alternatif------------------------------<br>");
			
			$list_val_perhitungan_matrix = array();
			$list_count_perhitungan_matrix = array();
			// $list_count_perhitungan_matrix_vs_prior = array();
			foreach ($list_per_dsn as $r_per_kri => $v_per_kri) {
				// $list_tmp_perhitungan_matrix_vs_prior = 0;
				foreach ($v_per_kri as $r_row_per_kri => $v_row_per_kri) {
					$temp_count_per_kri = 0;
					foreach ($v_row_per_kri as $r_col_per_kri => $v_col_per_kri) {
						$temp_per_kri = $v_col_per_kri / $count_list_kri_dsn[$r_per_kri][$r_col_per_kri];
						
						$list_val_perhitungan_matrix[$r_per_kri][$r_row_per_kri][$r_col_per_kri] = $temp_per_kri;

						
						$temp_count_per_kri += $temp_per_kri; 
						
					}

					$list_count_perhitungan_matrix[$r_row_per_kri][$r_per_kri] = $temp_count_per_kri/$count_dsn ;
					
				}
				
			}

			// print_r("#----------------------------Perhitungan Perbandingan Antar Alternatif------------------------------<br>");
			// $data["list_val_perhitungan_matrix"] = $list_val_perhitungan_matrix;
			// print_r("#----------------------------Total Row Perbandingan Antar Alternatif------------------------------<br>");
			// $data["list_count_perhitungan_matrix"] = $list_count_perhitungan_matrix;
			// print_r("#-------------------------------Table Priority---------------------------<br>");
			

		#------------- Perhitungan Perbandingan Antar Alternatif -------------

		
		#------------- Perhitungan Akhir : -------------
			$val_per_plus_id = array();
			$perhitungan_hasil = array();
			foreach ($list_count_perhitungan_matrix as $r_list_count_perhitungan_matrix => $v_list_count_perhitungan_matrix) {
				$tmp_col_list_count_perhitungan_matrix = 0;
				foreach ($v_list_count_perhitungan_matrix as $r_col_list_count_perhitungan_matrix => $v_col_list_count_perhitungan_matrix) {
					$tmp_col_list_count_perhitungan_matrix += $v_col_list_count_perhitungan_matrix*$list_priority[$r_col_list_count_perhitungan_matrix];
				}
				$val_per_plus_id[$r_list_count_perhitungan_matrix]["val"] = number_format($tmp_col_list_count_perhitungan_matrix, 5, '.','');
				$val_per_plus_id[$r_list_count_perhitungan_matrix]["nim"] = $id_r_dsn[$r_list_count_perhitungan_matrix];


				$perhitungan_hasil[$r_list_count_perhitungan_matrix] = number_format($tmp_col_list_count_perhitungan_matrix, 5, '.','');
			}

			// print_r("#-------------------- Perhitungan Akhir : ------------------<br>");
			
			$data["perhitungan_hasil"] = $perhitungan_hasil;
			$data["perhitungan_hasil_all"] = $val_per_plus_id;
			
		#------------- Perhitungan Akhir : -------------

			$response = array("status" => true, "data" => $data);

			}else {
				$response = array("status" => false, "data" => null);
				// echo "tidak konsisten";
			}

			// $this->load->view("ahp/main_hasil", $data);

			print_r(json_encode($response));
	}

	public function process_pilihan(){
		// print_r("<pre>");
		// print_r($_POST);
		$list_dsn = $this->input->post("list_dsn");
		// $data_list_dsn = array("12.12.9900","13.31.0063","53.12.1144");

		if(!empty($list_dsn)){
			$data_list_dsn = json_decode($list_dsn);
		}

		// $data["page"] = "main_ahp";

		#------------- set basic response -------------

		$response = array("status" => false, "data" => null);

		#------------- set basic response -------------

		$kri = $this->ma->get_kri();
		$data["kri"] = $kri;
		$ri = $this->ma->get_ri();
		$data["ri"] = $ri;
		// print_r("<pre>");

		#------------- set basic var -------------

		$count_kri = count($kri);
		$count_col_per = array();
		
		$list_id_col = array();
		$list_val_col = array();

		$list_id_row = array();
		$list_val_row = array();

		$list_id_kri = array();
		$list_tipe_kri = array();
		#------------- set basic var -------------

		#------------- set perbandingan var -------------


		foreach ($kri as $r_kri => $val_kri) {
			// print_r($val_kri);
			$data_perbandingan_col = $this->ma->get_perbandingan(array("col_per"=>$val_kri->id_kri));
			// print_r($da1ta_perbandingan_col);
			$temp_col = array();
			$temp_id_col = array();

			$list_id_kri[$r_kri] = $val_kri->ket_kri;
			foreach ($data_perbandingan_col as $val_col) {
				array_push($temp_col, $val_col->val_per);
				array_push($temp_id_col, $val_col->id_per);
			}

			

			$list_val_col[$val_kri->id_kri] = $temp_col;
			$list_id_col[$val_kri->id_kri] = $temp_id_col;

			$data_perbandingan_row = $this->ma->get_perbandingan(array("row_per"=>$val_kri->id_kri));
			
			$temp_row = array();
			$temp_id_row = array();
			foreach ($data_perbandingan_row as $val_row) {
				array_push($temp_row, $val_row->val_per);
				array_push($temp_id_row, $val_row->id_per);
			}

			$list_val_row[$val_kri->id_kri] = $temp_row;
			$list_id_row[$val_kri->id_kri] = $temp_id_row;
			
			
			array_push($count_col_per, array_sum($temp_col));
			array_push($list_tipe_kri, $val_kri->tipe_kri);
			
		}

		#------------- set perbandingan var -------------
		// $data["list_tipe_kri"] = $list_tipe_kri;
		
		// echo "#------------- Kriteria -------------<br>";
		
		// $data["list_id_kri"] = $list_id_kri;
		
		// echo "#------------- set perbandingan var -------------<br>";
		
		// $data["set_perbandingan_awal"] = $list_val_row;
		

		#------------- set priority var -------------
		$list_new_matrix = array();
		$list_priority = array();
		
		foreach ($kri as $r_kri =>$val_kri) {
			$sum_row_temp = 0;
			foreach ($list_val_row[$val_kri->id_kri] as $r_list_row => $val_list_row) {
				$val_rata_perbandingan = number_format($val_list_row / $count_col_per[$r_list_row], 4, ".","");
				$list_new_matrix[$r_kri][$r_list_row] = number_format($val_list_row / $count_col_per[$r_list_row], 4, ".","");
				
				$sum_row_temp += $val_rata_perbandingan;
			}

			array_push($list_priority, $sum_row_temp/$count_kri);
			
		}
		// echo "#------------- set new matrix -------------<br>";
		
		// $data["set_new_matrix"] = $list_new_matrix;
		// echo "#------------- set priority var -------------<br>";
		
		// $data["set_priority"] = $list_priority;

		#------------- set priority var -------------


		#------------- set kriteria vs rata-rata var -------------

		$list_kri_vs_rata = array();
		foreach ($kri as $val_kri) {
			$count_data_n = 0;
			foreach ($list_val_row[$val_kri->id_kri] as $r_list_row => $val_list_row) {
				$data_n = $val_list_row * $list_priority[$r_list_row];
				$count_data_n += $data_n;
				
			}
			
			array_push($list_kri_vs_rata, $count_data_n);
		}
		
		// $data["list_kri_vs_rata"] = $list_kri_vs_rata;

		#------------- set kriteria vs rata-rata var -------------



		#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------
			$count_kri_vs_rata = 0;
			foreach ($list_kri_vs_rata as $r_kri_vs_rata =>$val_kri_vs_rata) {
				$count_kri_vs_rata += $val_kri_vs_rata/$list_priority[$r_kri_vs_rata];
			}

			$count_kri_vs_rata = $count_kri_vs_rata/$count_kri;
			// $data["count_kri_vs_rata"]=$count_kri_vs_rata;
			
		#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------

		#------------- Indeks Consistensi -------------
			$konsistensi = ($count_kri_vs_rata - $count_kri)/($count_kri-1);
			// $data["konsistensi"]=$konsistensi;
			
		#------------- Indeks Consistensi -------------
												
		#------------- Indeks Consistensi / ri -------------
			$konsistensi_ri = $konsistensi / $ri[$count_kri];
			// $data["konsistensi_ri"]=$konsistensi_ri;
			
		#------------- Indeks Consistensi -------------

			if($konsistensi_ri <= 0.1){
#------------- Kriteria Dosen -------------
			$dsn = $this->md->dsn_get();
			$count_dsn = count($dsn);

			$id_r_dsn = array();
			$matrix_penilaian_dsn = array();

			$data_dsn = array();

			$penilainan_all = array();
			$id_array_dsn = 0;
			$count_t_option = 0;
			foreach ($dsn as $r_dsn => $val_dsn) {
				if(in_array($val_dsn->nidn, $data_list_dsn)){
					array_push($id_r_dsn, $val_dsn->nidn);
					$data_dsn[$val_dsn->nidn] = $val_dsn;
					$penilaian_dsn= $this->md->get_all_penilaian(array("nidn"=>$val_dsn->nidn))->result();
					foreach ($penilaian_dsn as $r_penilaian_dsn => $val_penilaian_dsn) {
						$matrix_penilaian_dsn[$r_penilaian_dsn][$r_dsn] = $val_penilaian_dsn->val_sub;

					}
					$count_t_option++;
				}

				// print_r($penilaian_dsn);
				// print_r("----------------------------------------------------------");
			}

			// print_r("#----------------------------Dosen------------------------------<br>");
			// print_r($id_r_dsn);
			// print_r($data_dsn);
			// $data["id_r_dsn"] = $id_r_dsn;
			$data["data_dsn"] = $data_dsn;
			// print_r("#----------------------------Tipe Kriteria------------------------------<br>");
			// print_r($list_tipe_kri);

			// print_r("#----------------------------Kriteria Dosen------------------------------<br>");
			// print_r($matrix_penilaian_dsn);
			// $data["matrix_penilaian_dsn"] = $matrix_penilaian_dsn;
			
		#------------- Kriteria Dosen -------------		

		#------------- Perbandingan Kriteria Dosen -------------
			$list_per_dsn = array();
			$count_list_kri_dsn = array();
			foreach ($matrix_penilaian_dsn as $r_matrix_penilaian_dsn => $val_matrix_penilaian_dsn) {
				$count_list_kri_dsn[$r_matrix_penilaian_dsn] = array();
				foreach ($val_matrix_penilaian_dsn as $r_row_matrix_penilaian_dsn => $v_row_matrix_penilaian_dsn) {
					foreach ($val_matrix_penilaian_dsn as $r_col_matrix_penilaian_dsn => $v_col_matrix_penilaian_dsn) {

						$temp_list_per_dsn = 0;
						if($list_tipe_kri[$r_matrix_penilaian_dsn] == 1){
							$temp_list_per_dsn = $v_col_matrix_penilaian_dsn/$v_row_matrix_penilaian_dsn;
						}else {
							$temp_list_per_dsn = $v_row_matrix_penilaian_dsn/$v_col_matrix_penilaian_dsn;
						}
						
						$list_per_dsn[$r_matrix_penilaian_dsn][$r_row_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] = $temp_list_per_dsn;

						if(array_key_exists($r_col_matrix_penilaian_dsn, $count_list_kri_dsn[$r_matrix_penilaian_dsn])){
							$count_list_kri_dsn[$r_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] += $temp_list_per_dsn;

						}else {
							$count_list_kri_dsn[$r_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] = $temp_list_per_dsn;

						}	
					}
				}
			}
			// print_r("#----------------------------Perbandingan Kriteria Dosen------------------------------<br>");
			// print_r($list_per_dsn);
			// $data["list_per_dsn"] = $list_per_dsn; 

			// print_r("#----------------------------Total Perbandingan Kriteria Dosen------------------------------<br>");
			// print_r($count_list_kri_dsn);
			// $data["count_list_kri_dsn"] = $count_list_kri_dsn; 

			
		#------------- Perbandingan Kriteria Dosen -------------		

		#------------- Perhitungan Perbandingan Antar Alternatif -------------

			// print_r("#----------------------------Perhitungan Perbandingan Antar Alternatif------------------------------<br>");
			
			$list_val_perhitungan_matrix = array();
			$list_count_perhitungan_matrix = array();
			// $list_count_perhitungan_matrix_vs_prior = array();
			foreach ($list_per_dsn as $r_per_kri => $v_per_kri) {
				// $list_tmp_perhitungan_matrix_vs_prior = 0;
				foreach ($v_per_kri as $r_row_per_kri => $v_row_per_kri) {
					$temp_count_per_kri = 0;
					foreach ($v_row_per_kri as $r_col_per_kri => $v_col_per_kri) {
						$temp_per_kri = $v_col_per_kri / $count_list_kri_dsn[$r_per_kri][$r_col_per_kri];
						
						$list_val_perhitungan_matrix[$r_per_kri][$r_row_per_kri][$r_col_per_kri] = $temp_per_kri;

						
						$temp_count_per_kri += $temp_per_kri; 
						
					}

					$list_count_perhitungan_matrix[$r_row_per_kri][$r_per_kri] = $temp_count_per_kri/$count_t_option ;
					
				}
				
			}

			// print_r("#----------------------------Perhitungan Perbandingan Antar Alternatif------------------------------<br>");
			// // $data["list_val_perhitungan_matrix"] = $list_val_perhitungan_matrix;
			// print_r($list_val_perhitungan_matrix);
			// print_r("#----------------------------Total Row Perbandingan Antar Alternatif------------------------------<br>");
			// // $data["list_count_perhitungan_matrix"] = $list_count_perhitungan_matrix;
			// print_r($list_count_perhitungan_matrix);
			// print_r("#-------------------------------Table Priority---------------------------<br>");
			

		#------------- Perhitungan Perbandingan Antar Alternatif -------------

		
		#------------- Perhitungan Akhir : -------------
			$val_per_plus_id = array();
			$perhitungan_hasil = array();

			$no_hasil_akhir = 0;
			foreach ($list_count_perhitungan_matrix as $r_list_count_perhitungan_matrix => $v_list_count_perhitungan_matrix) {
				$tmp_col_list_count_perhitungan_matrix = 0;
				foreach ($v_list_count_perhitungan_matrix as $r_col_list_count_perhitungan_matrix => $v_col_list_count_perhitungan_matrix) {
					$tmp_col_list_count_perhitungan_matrix += $v_col_list_count_perhitungan_matrix*$list_priority[$r_col_list_count_perhitungan_matrix];
				}
				$val_per_plus_id[$no_hasil_akhir]["val"] = number_format($tmp_col_list_count_perhitungan_matrix, 5, '.','');
				$val_per_plus_id[$no_hasil_akhir]["nim"] = $id_r_dsn[$no_hasil_akhir];
				// print_r("---------<br>");
				// print_r($r_list_count_perhitungan_matrix);


				$perhitungan_hasil[$no_hasil_akhir] = number_format($tmp_col_list_count_perhitungan_matrix, 5, '.','');

				$no_hasil_akhir++;
			}

			// print_r("#-------------------- Perhitungan Akhir : ------------------<br>");
			
			
			$data["perhitungan_hasil_all"] = $val_per_plus_id;
			$data["perhitungan_hasil"] = $perhitungan_hasil;
			// foreach ($perhitungan_hasil as $r_perhitungan_hasil => $v_perhitungan_hasil) {
			// 	array_push($data["perhitungan_hasil"], $v_perhitungan_hasil);
			// }
			
		#------------- Perhitungan Akhir : -------------

			$response = array("status" => true, "data" => $data);

			}else {
				$response = array("status" => false, "data" => null);
				// echo "tidak konsisten";
			}

			// $this->load->view("ahp/main_hasil", $data);

			print_r(json_encode($response));

	}

	public function index_pilihan(){
		$data["page"] = "main_ahp";

		$data_pilihan = $this->input->get("pilihan");
		if(strpos($data_pilihan, "-")){
			$data_list_dsn = explode("-", $data_pilihan);
		}else {
			$data_list_dsn = array($data_pilihan);

		}
		// print_r($data_list_dsn);

		// $data_list_dsn = array("12.12.9900","13.31.0063","53.12.1144");

		$kri = $this->ma->get_kri();
		$data["kri"] = $kri;
		$ri = $this->ma->get_ri();
		$data["ri"] = $ri;
		print_r("<pre>");

		#------------- set basic var -------------

		$count_kri = count($kri);
		$count_col_per = array();
		
		$list_id_col = array();
		$list_val_col = array();

		$list_id_row = array();
		$list_val_row = array();

		$list_id_kri = array();
		$list_tipe_kri = array();
		#------------- set basic var -------------

		#------------- set perbandingan var -------------


		foreach ($kri as $r_kri => $val_kri) {
			// print_r($val_kri);
			$data_perbandingan_col = $this->ma->get_perbandingan(array("col_per"=>$val_kri->id_kri));
			// print_r($da1ta_perbandingan_col);
			$temp_col = array();
			$temp_id_col = array();

			$list_id_kri[$r_kri] = $val_kri->ket_kri;
			foreach ($data_perbandingan_col as $val_col) {
				array_push($temp_col, $val_col->val_per);
				array_push($temp_id_col, $val_col->id_per);
			}

			// array_push($list_val_col, $temp_col);
			// array_push($list_id_col, $temp_id_col);

			$list_val_col[$val_kri->id_kri] = $temp_col;
			$list_id_col[$val_kri->id_kri] = $temp_id_col;

			$data_perbandingan_row = $this->ma->get_perbandingan(array("row_per"=>$val_kri->id_kri));
			// print_r($da1ta_perbandingan_col);
			$temp_row = array();
			$temp_id_row = array();
			foreach ($data_perbandingan_row as $val_row) {
				array_push($temp_row, $val_row->val_per);
				array_push($temp_id_row, $val_row->id_per);
			}

			$list_val_row[$val_kri->id_kri] = $temp_row;
			$list_id_row[$val_kri->id_kri] = $temp_id_row;
			
			// $count_col_per[$val_kri->id_kri] = array_sum($temp_col);
			array_push($count_col_per, array_sum($temp_col));
			array_push($list_tipe_kri, $val_kri->tipe_kri);
			// print_r("--------------kriteria------------<br>");
			// print_r($val_kri->id_kri);
			// print_r($count_col_per);
			// print_r("--------------kriteria------------<br><br>");
						// array_push($list_val_row, $temp_row);
			// array_push($list_id_row, $temp_id_row);
		}

		#------------- set perbandingan var -------------
		$data["list_tipe_kri"] = $list_tipe_kri;
		// print_r($list_id_col);
		// print_r($list_val_col);
		// echo "#------------- Kriteria -------------<br>";
		// print_r($list_id_kri);
		$data["list_id_kri"] = $list_id_kri;
		// print_r($list_id_row);
		// echo "#------------- set perbandingan var -------------<br>";
		// print_r($list_val_row);
		$data["set_perbandingan_awal"] = $list_val_row;
		// print_r($count_col_per);
		// $this->load->view('main_index',$data);

		#------------- set priority var -------------
		$list_new_matrix = array();
		$list_priority = array();
		// $list_sum_row = array();
		// $priority_row_tmp = array();
		foreach ($kri as $r_kri =>$val_kri) {
			$sum_row_temp = 0;
			foreach ($list_val_row[$val_kri->id_kri] as $r_list_row => $val_list_row) {
				$val_rata_perbandingan = number_format($val_list_row / $count_col_per[$r_list_row], 4, ".","");
				$list_new_matrix[$r_kri][$r_list_row] = number_format($val_list_row / $count_col_per[$r_list_row], 4, ".","");
				// print_r($count_col_per[$r_list_row]);
				// print_r(" / ");
				// print_r($val_list_row);
				// print_r(" --- ");
				// print_r($val_rata_perbandingan);
				// print_r(" | ");
				// array_push($priority_row_tmp, $val_rata_perbandingan);
				$sum_row_temp += $val_rata_perbandingan;
			}

			// $list_priority[$val_kri->id_kri] = $sum_row_temp/$count_kri;
			array_push($list_priority, $sum_row_temp/$count_kri);
			// print_r(" <---> ");
			// print_r($sum_row_temp/$count_kri);
			// echo "<br>";
		}
		// echo "#------------- set new matrix -------------<br>";
		// print_r($list_new_matrix);
		$data["set_new_matrix"] = $list_new_matrix;
		// echo "#------------- set priority var -------------<br>";
		// print_r($list_priority);
		$data["set_priority"] = $list_priority;

		#------------- set priority var -------------


		#------------- set kriteria vs rata-rata var -------------

		$list_kri_vs_rata = array();
		foreach ($kri as $val_kri) {
			$count_data_n = 0;
			foreach ($list_val_row[$val_kri->id_kri] as $r_list_row => $val_list_row) {
				$data_n = $val_list_row * $list_priority[$r_list_row];
				$count_data_n += $data_n;
				
			}
			// print_r($count_data_n);
			// print_r(" --- ");
			array_push($list_kri_vs_rata, $count_data_n);
		}
		// print_r("#------------- set kriteria vs rata-rata var -------------<br>");
		// print_r($list_kri_vs_rata);
		$data["list_kri_vs_rata"] = $list_kri_vs_rata;

		#------------- set kriteria vs rata-rata var -------------



		#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------
			$count_kri_vs_rata = 0;
			foreach ($list_kri_vs_rata as $r_kri_vs_rata =>$val_kri_vs_rata) {
				$count_kri_vs_rata += $val_kri_vs_rata/$list_priority[$r_kri_vs_rata];
			}

			$count_kri_vs_rata = $count_kri_vs_rata/$count_kri;
			$data["count_kri_vs_rata"]=$count_kri_vs_rata;
			// print_r("#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------<br>");
			// print_r($count_kri_vs_rata);
			// echo "<br>";
		#------------- set (kriteria vs rata-rata)/Priority Vektor var -------------

		#------------- Indeks Consistensi -------------
			$konsistensi = ($count_kri_vs_rata - $count_kri)/($count_kri-1);
			$data["konsistensi"]=$konsistensi;
			// print_r("#------------- Indeks Consistensi -------------<br>");
			// print_r($konsistensi);
			// echo "<br>";
		#------------- Indeks Consistensi -------------
												
		#------------- Indeks Consistensi / ri -------------
			$konsistensi_ri = $konsistensi / $ri[$count_kri];
			$data["konsistensi_ri"]=$konsistensi_ri;
			// print_r("#------------- Indeks Consistensi / ri -------------<br>");
			// print_r($konsistensi_ri);
			// echo "<br>";


		#------------- Indeks Consistensi -------------

			if($konsistensi_ri <= 0.1){
#------------- Kriteria Dosen -------------
			$dsn = $this->md->dsn_get();
			$count_dsn = count($dsn);

			$id_r_dsn = array();
			$matrix_penilaian_dsn = array();

			$id_array_dsn = 0;
			$penilainan_all = array();

			$count_t_option = 0;
			foreach ($dsn as $r_dsn => $val_dsn) {
				if(in_array($val_dsn->nidn, $data_list_dsn)){
					$id_r_dsn[$r_dsn] = $val_dsn->nidn;
					// array_push($id_r_dsn, $val_dsn->nidn);
					$penilaian_dsn= $this->md->get_all_penilaian(array("nidn"=>$val_dsn->nidn))->result();
					foreach ($penilaian_dsn as $r_penilaian_dsn => $val_penilaian_dsn) {
						$matrix_penilaian_dsn[$r_penilaian_dsn][$r_dsn] = $val_penilaian_dsn->val_sub;

					}
					$count_t_option++;
				}
				// print_r($penilaian_dsn);
				// print_r("----------------------------------------------------------");
			}

			// print_r("#----------------------------Dosen------------------------------<br>");
			// print_r($id_r_dsn);
			$data["id_r_dsn"] = $id_r_dsn;
			
			// print_r("#----------------------------Tipe Kriteria------------------------------<br>");
			// print_r($list_tipe_kri);

			// print_r("#----------------------------Kriteria Dosen------------------------------<br>");
			// print_r($matrix_penilaian_dsn);
			$data["matrix_penilaian_dsn"] = $matrix_penilaian_dsn;
			
		#------------- Kriteria Dosen -------------		

		#------------- Perbandingan Kriteria Dosen -------------
			$list_per_dsn = array();
			$count_list_kri_dsn = array();
			foreach ($matrix_penilaian_dsn as $r_matrix_penilaian_dsn => $val_matrix_penilaian_dsn) {
				$count_list_kri_dsn[$r_matrix_penilaian_dsn] = array();
				foreach ($val_matrix_penilaian_dsn as $r_row_matrix_penilaian_dsn => $v_row_matrix_penilaian_dsn) {
					foreach ($val_matrix_penilaian_dsn as $r_col_matrix_penilaian_dsn => $v_col_matrix_penilaian_dsn) {

						$temp_list_per_dsn = 0;
						if($list_tipe_kri[$r_matrix_penilaian_dsn] == 1){
							// $list_per_dsn[$r_matrix_penilaian_dsn][$r_row_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] 
							$temp_list_per_dsn = $v_col_matrix_penilaian_dsn/$v_row_matrix_penilaian_dsn;
						}else {
							$temp_list_per_dsn = $v_row_matrix_penilaian_dsn/$v_col_matrix_penilaian_dsn;
						}
						
						$list_per_dsn[$r_matrix_penilaian_dsn][$r_row_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] = $temp_list_per_dsn;

						if(array_key_exists($r_col_matrix_penilaian_dsn, $count_list_kri_dsn[$r_matrix_penilaian_dsn])){
							$count_list_kri_dsn[$r_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] += $temp_list_per_dsn;

							// $v_col_matrix_penilaian_dsn/$v_row_matrix_penilaian_dsn;
							// echo "ada<br>";
						}else {
							// echo "gak ada<br>";
							$count_list_kri_dsn[$r_matrix_penilaian_dsn][$r_col_matrix_penilaian_dsn] = $temp_list_per_dsn;

							// $v_col_matrix_penilaian_dsn/$v_row_matrix_penilaian_dsn;
						}	
					}
				}
			}
			// print_r("#----------------------------Perbandingan Kriteria Dosen------------------------------<br>");
			// print_r($list_per_dsn);
			$data["list_per_dsn"] = $list_per_dsn; 

			// print_r("#----------------------------Total Perbandingan Kriteria Dosen------------------------------<br>");
			// print_r($count_list_kri_dsn);
			$data["count_list_kri_dsn"] = $count_list_kri_dsn; 

			
		#------------- Perbandingan Kriteria Dosen -------------		

		#------------- Perhitungan Perbandingan Antar Alternatif -------------

			// print_r("#----------------------------Perhitungan Perbandingan Antar Alternatif------------------------------<br>");
			
			$list_val_perhitungan_matrix = array();
			$list_count_perhitungan_matrix = array();
			// $list_count_perhitungan_matrix_vs_prior = array();
			foreach ($list_per_dsn as $r_per_kri => $v_per_kri) {
				// $list_tmp_perhitungan_matrix_vs_prior = 0;
				foreach ($v_per_kri as $r_row_per_kri => $v_row_per_kri) {
					$temp_count_per_kri = 0;
					foreach ($v_row_per_kri as $r_col_per_kri => $v_col_per_kri) {
						$temp_per_kri = $v_col_per_kri / $count_list_kri_dsn[$r_per_kri][$r_col_per_kri];
						
						$list_val_perhitungan_matrix[$r_per_kri][$r_row_per_kri][$r_col_per_kri] = $temp_per_kri;

						// echo  $temp_per_kri. " | ";
						$temp_count_per_kri += $temp_per_kri; 
						// print_r();
					}

					$list_count_perhitungan_matrix[$r_row_per_kri][$r_per_kri] = $temp_count_per_kri/$count_t_option ;
					// echo $temp_count_per_kri/$count_dsn;
					// echo "<br>";
					// $list_tmp_perhitungan_matrix_vs_prior += ($temp_count_per_kri/$count_dsn) * $list_priority[$r_row_per_kri];
				}
				// echo $list_tmp_perhitungan_matrix_vs_prior;
				// echo "<br>";
				// $list_count_perhitungan_matrix_vs_prior[$r_per_kri] = $list_tmp_perhitungan_matrix_vs_prior;
				// echo "<br><hr><br>";
			}

			// print_r("#----------------------------Perhitungan Perbandingan Antar Alternatif------------------------------<br>");
			// print_r($list_val_perhitungan_matrix);
			$data["list_val_perhitungan_matrix"] = $list_val_perhitungan_matrix;
			// print_r("#----------------------------Total Row Perbandingan Antar Alternatif------------------------------<br>");
			// print_r($list_count_perhitungan_matrix);
			$data["list_count_perhitungan_matrix"] = $list_count_perhitungan_matrix;
			// print_r("#-------------------------------Table Priority---------------------------<br>");
			// print_r($list_priority);


			// print_r("----------------------------------------------------------<br>");
			// print_r($list_count_perhitungan_matrix_vs_prior);

		#------------- Perhitungan Perbandingan Antar Alternatif -------------

		
		#------------- Perhitungan Akhir : -------------
			$perhitungan_hasil = array();
			foreach ($list_count_perhitungan_matrix as $r_list_count_perhitungan_matrix => $v_list_count_perhitungan_matrix) {
				$tmp_col_list_count_perhitungan_matrix = 0;
				foreach ($v_list_count_perhitungan_matrix as $r_col_list_count_perhitungan_matrix => $v_col_list_count_perhitungan_matrix) {
					$tmp_col_list_count_perhitungan_matrix += $v_col_list_count_perhitungan_matrix*$list_priority[$r_col_list_count_perhitungan_matrix];
				}
				$perhitungan_hasil[$r_list_count_perhitungan_matrix] = number_format($tmp_col_list_count_perhitungan_matrix, 5, '.','');
			}

			// print_r("#-------------------- Perhitungan Akhir : ------------------<br>");
			// print_r($perhitungan_hasil);
			// $sort = asort($perhitungan_hasil, SORT_NUMERIC);
			// print_r($sort);
			$data["perhitungan_hasil"] = $perhitungan_hasil;
			// print_r("----------------------------------------------------------<br>");

		#------------- Perhitungan Akhir : -------------

			}else {
				echo "tidak konsisten";
			}

			// print_r($list_tipe_kri);
			// print_r($data);
			$this->load->view("ahp/main_hasil", $data);
	}
}


