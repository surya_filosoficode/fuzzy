<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindsn extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("kriteria", "kri");
		$this->load->model("main_dsn", "md");
		$this->load->model("main/mainmodel", "mm");

        $this->load->library("response_message");
        $this->load->library("encrypt");

        if(isset($_SESSION["admin_lv_1"])){
			if($this->session->userdata("admin_lv_1")["is_log"] != 1){
				redirect(base_url()."admin/login");
			}
		}else{
			redirect(base_url()."admin/login");
		}
        
	}

#=================================================================================================#
#-------------------------------------------main_dosen--------------------------------------------#
#=================================================================================================#

	public function index(){
		$data["page"] = "page_dosen";
		$data["list_data"] = $this->mm->get_data_all("dsn");
		$this->load->view("index",$data);
	}
#=================================================================================================#
#-------------------------------------------main_dosen--------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------insert_dosen------------------------------------------#
#=================================================================================================#
	private function validate_input_dosen(){
        $config_val_input = array(
                array(
                    'field'=>'nama_dsn',
                    'label'=>'Nama Dosen',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nidn',
                    'label'=>'Nomor induk Dosen',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'alamat',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tlp',
                    'label'=>'Telepon',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_dosen(){
    	$detail_msg = array("nama_dsn"=>"", "nidn"=>"", "alamat"=>"", "tlp"=>"");
    	$main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

    	if($this->validate_input_dosen()){
    		$nama_dsn 	= $this->input->post("nama_dsn");
    		$nidn 		= $this->input->post("nidn");
    		$alamat 	= $this->input->post("alamat");
    		$tlp 		= $this->input->post("tlp");

    		$time_add 	= date("Y-m-d h:i:s");
    		$id_admin	= $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

    		$insert = $this->db->query("SELECT insert_dsn('".$nama_dsn."', '".$nidn."', '".$alamat."', '".$tlp."') AS id_dsn;")->row_array();
    		if($insert){
    			$main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
    		}
    	}else {
    		$detail_msg["nama_dsn"] = strip_tags(form_error("nama_dsn"));
    		$detail_msg["nidn"] 	= strip_tags(form_error("nidn"));
    		$detail_msg["alamat"] 	= strip_tags(form_error("alamat"));
    		$detail_msg["tlp"] 		= strip_tags(form_error("tlp"));
    	}

    	$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
    	print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------insert_dosen------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------get_dsn_update----------------------------------------#
#=================================================================================================#
    public function get_dsn_update(){
        $id = $this->input->post("id_dsn");
        $data = $this->mm->get_data_each("dsn",array("id_dsn"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }
#=================================================================================================#
#-------------------------------------------get_dsn_update----------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------update_dosen------------------------------------------#
#=================================================================================================#
	private function validate_update_dosen(){
        $config_val_input = array(
                array(
                    'field'=>'nama_dsn',
                    'label'=>'Nama Dosen',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nidn',
                    'label'=>'Nomor induk Dosen',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'alamat',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tlp',
                    'label'=>'Telepon',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_dosen(){
    	$detail_msg = array("nama_dsn"=>"", "nidn"=>"", "alamat"=>"", "tlp"=>"");
    	$main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));

    	if($this->validate_update_dosen()){
    		$nama_dsn 	= $this->input->post("nama_dsn");
    		$nidn 		= $this->input->post("nidn");
    		$alamat 	= $this->input->post("alamat");
    		$tlp 		= $this->input->post("tlp");

    		$id_dsn 	= $this->input->post("id_dsn");

    		$time_add 	= date("Y-m-d h:i:s");
    		$id_admin	 = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

    		$data_set = array("nama"=> $nama_dsn,
    							"nidn"=> $nidn,
    							"alamat"=> $alamat,
    							"tlp"=> $tlp);
    		$data_where = array("id_dsn"=>$id_dsn);

    		$insert = $this->mm->update_data("dsn", $data_set, $data_where);
    		if($insert){
    			$main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
    		}
    	}else {
    		$detail_msg["nama_dsn"] = strip_tags(form_error("nama_dsn"));
    		$detail_msg["nidn"] 	= strip_tags(form_error("nidn"));
    		$detail_msg["alamat"] 	= strip_tags(form_error("alamat"));
    		$detail_msg["tlp"] 		= strip_tags(form_error("tlp"));
    	}

    	$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
    	print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------update_dosen------------------------------------------#
#=================================================================================================#
	
#=================================================================================================#
#-------------------------------------------delete_dosen------------------------------------------#
#=================================================================================================#
	public function delete_dsn(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        
        $id_dsn =$this->input->post("id_dsn");

        $data_where = array("id_dsn"=> $id_dsn);
        if($this->mm->delete_data("penilaian", array("nidn"=>$id_dsn))){
            if($this->mm->delete_data("dsn", $data_where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=================================================================================================#
#-------------------------------------------delete_dosen------------------------------------------#
#=================================================================================================#
}
