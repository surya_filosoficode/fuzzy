<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainsk extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Main_dsn", "md");
		$this->load->model("Main_sk", "msk");
        $this->load->model("Main_mhs", "mhs");

        $this->load->library("response_message");
        
        if(isset($_SESSION["admin_lv_1"])){
            if($this->session->userdata("admin_lv_1")["is_log"] != 1){
                redirect(base_url()."admin/login");
            }
        }else{
            redirect(base_url()."admin/login");
        }
	}

    public function livesearch(){
        $id = $this->input->post("nim");
        // print_r($id);
        $mhs = $this->mhs->mhs_like_mhs($id);
        if(!empty($mhs)){
            foreach ($mhs as $r_mhs => $v_mhs) {
                echo "<a href=\"#\" onclick=\"click_select('".$v_mhs->nim."')\"> ".$v_mhs->nim." - ".$v_mhs->nama."</a><br>";
            }    
        }
        
        // print_r($mhs);
    }

	public function index()
	{
		$data["sk"] = $this->msk->sk_get();
        $data["dsn"] = $this->md->dsn_get();

		$data["page"] = "sk";
		$this->load->view('main_index',$data);
	}

	#----------------------------------------------------------------------------------------------------------------Data SKRIPSI-----------------------------------------------------------------------------------------------------------
	

	private function validation_sk(){
		$config_val_input = array(
                array(
                    'field'=>'nim',
                    'label'=>'NIM',
                    'rules'=>'required|is_unique[skripsi.nim]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("NIM_AVAIL")
                    )
                       
                ),array(
                    'field'=>'us1',
                    'label'=>'Usulan 2',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'us2',
                    'label'=>'Usulan 1',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'dsn_pilihan',
                    'label'=>'Dosen Pilihan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	// public function insert_skripsi(){
	// 	if($this->validation_sk()){
	// 		$nim = $this->input->post("id_nim");
	// 		$us2 = $this->input->post("us1");
	// 		$us1 = $this->input->post("us2");
	// 		$pem1 = $this->input->post("pem1");
	// 		$pem2 = $this->input->post("pem2");
	// 		$pem3 = $this->input->post("pem3");

	// 		$send = array(
	// 				"nim"=>$nim,
	// 				"sk1"=>$us1,
	// 				"sk2"=>$us2,
	// 				"dospem1"=>$pem1,
	// 				"dospem2"=>$pem2,
	// 				"dospem3"=>$pem3
	// 			);
 //            if($this->msk->sk_insert($send)){
 //                echo "masuk pak eko";
 //            }else {
 //                echo "gagal pak eko";
 //            }
	// 	}else {
 //            echo "input invalid";
 //            echo validation_errors();
	// 	}		
	// }

	// public function update_skripsi(){
	// 	if($this->validation_sk_up()){
 //            $id_sk = $this->input->post("id_sk");
 //            $nim = $this->input->post("id_nim");
 //            $us2 = $this->input->post("us1");
 //            $us1 = $this->input->post("us2");
 //            $pem1 = $this->input->post("pem1");
 //            $pem2 = $this->input->post("pem2");
 //            $pem3 = $this->input->post("pem3");

 //            $send = array(
 //                    "sk1"=>$us1,
 //                    "sk2"=>$us2,
 //                    "dospem1"=>$pem1,
 //                    "dospem2"=>$pem2,
 //                    "dospem3"=>$pem3
 //                );
 //            if($this->msk->sk_update($send, array("id_sk"=>$id_sk, "nim"=>$nim))){
 //                echo "masuk pak eko";
 //            }else {
 //                echo "gagal pak eko";
 //            }
 //        }else {
 //            echo "input invalid";
 //        }   
	// }

	public function delete_skripsi($id_sk){
		if($this->msk->sk_delete(array("id_sk"=>$id_sk))){
            $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            $detail_msg = null;
        }else {
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
            $detail_msg = null;
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
        $this->session->set_flashdata("response_sk", $msg_array);
        redirect(base_url()."main/listsk");
	}

    public function create_json_mhs(){
        $nim = $this->input->post("nim");

        $data["mhs"] = $this->mhs->mhs_get_where(array("nim"=>$nim));
        // $data["sk"] = $this->msk->sk_get_where(array("nim"=>$nim));
        print_r(json_encode($data));
    }


#-------------------------------------------------------------------------hasil-----------------------------------------------------------------------------------

    public function insert_hasil(){
        if($this->validation_sk()){
            // print_r("<pre>");
            // print_r($_POST);
            $nim = $this->input->post("nim");
            $us1 = $this->input->post("us1");
            $us2 = $this->input->post("us2");
            $json_dsn_pilihan = $this->input->post("dsn_pilihan");

            $data = array(
                    "id_sk" => "",
                    "nim" => $nim,
                    "sk1" => $us1,
                    "sk2" => $us2,
                    "dospem" => implode("|",json_decode($json_dsn_pilihan))
                );
            
            if($this->msk->sk_insert($data)){
                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                $detail_msg = null;
            }else {
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                $detail_msg = null;
            }
        }else{
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $detail_msg = validation_errors();
            // print_r(validation_errors());
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);

        print_r(json_encode($msg_array));
        // $this->session->set_flashdata("response_skripsi", $msg_array);
    }



    public function list_sk(){
        $data["page"] = "list_sk";
        $data["sk"] = $this->msk->sk_get();
        $dsn = $this->md->dsn_get();

        $data["dsn"] = array();
        foreach ($dsn as $r_dsn => $v_dsn) {
            $data["dsn"][$v_dsn->nidn] = $v_dsn->nama; 
        }

        $this->load->view('main_index',$data);
    }
}
