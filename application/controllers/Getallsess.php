<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Getallsess extends CI_Controller {
	
	public function index()	{
		$this->load->library('pdf');
		$html = file_get_contents(base_url()."main/ahp");
	  	$this->pdf->loadHtml($html);
		$this->pdf->render();
		$this->pdf->stream("surya.pdf", array("Attachment"=>0));
	}
}
