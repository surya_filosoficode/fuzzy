<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainperhitungan extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("main/mainmodel", "mm");

        $this->load->library("response_message");
        
        if(isset($_SESSION["admin_lv_1"])){
			if($this->session->userdata("admin_lv_1")["is_log"] != 1){
				redirect(base_url()."admin/login");
			}
		}else{
			redirect(base_url()."admin/login");
		}
	}

	public function index_rules(){
		$master_kriteria = $this->mm->get_data_all_where("kriteria", array("sts_active"=>"0"));

		$count_kriteria = count($master_kriteria);
		$pow_kriteria = pow(2, $count_kriteria);

		$data_base_biner = array();
		if(file_exists("./assets/json_data/data_biner.json")){
			$str_json = file_get_contents("./assets/json_data/data_biner.json");
			$data_base_biner = json_decode($str_json);
		}

		#------------------------------------------data value rules-----------------------------#
				$str_table = "	<thead>
								<tr>
								
										<th width=\"".(100/($count_kriteria+1))."%\">No. Rule</th>";
				foreach ($master_kriteria as $k_master_kriteria => $v_master_kriteria) {
					$str_table .= "			<th width=\"".(100/($count_kriteria+1))."%\">".$v_master_kriteria->ket_kri."</th>";
				}
				
				$str_table .= "		</tr>
								</thead>";

		if(count($master_kriteria) >= 2){

				$str_table .= "	<tbody>";
				// print_r("<br><br>");
				$array_nilai_rules = array();
				$array_nilai_rules_min = array();

				$no_x = 1;
				for ($r=0; $r < $pow_kriteria ; $r++) { 
					$str_table .="	<tr>";
					$str_table .="		<td align=\"center\">".$no_x."</td>";
					for ($c=0; $c < $count_kriteria ; $c++) { 
						$str_biner = "Terendah";

						if($data_base_biner[$r][$c] == 1){
							$str_biner = "Tertinggi";
						}
						$str_table .="	<td align=\"center\">".$str_biner."</td>";
					}

					$str_table .="	</tr>";
					$no_x++;
				}
				$str_table .="	</tbody>";

			#------------------------------------------data value rules-----------------------------#

		}else{
			$str_table .= "	<tbody>";
			$str_table .="	<td colspan=\"".($count_kriteria+1)."\" align=\"center\">Aktifkan lebih 2 atau lebih krieria untuk melakukan proses</td>";
			$str_table .="	</tbody>";
		}
		$data["page"] = "page_rules";
		$data["str_table"] = $str_table;
		$this->load->view("index", $data);
	}


	public function index_analisa(){
		$master_kriteria = $this->mm->get_data_all_where("kriteria", array("sts_active"=>"0"));
		$master_dosen = $this->mm->get_data_all("dsn");
		$data_range_hasil = $this->mm->get_data_each("range_hasil", array());

		$count_kriteria = count($master_kriteria);
		$pow_kriteria = pow(2, $count_kriteria);

		// print_r($count_kriteria." - ".$pow_kriteria);

		$data_base_biner = array();
		if(file_exists("./assets/json_data/data_biner.json")){
			$str_json = file_get_contents("./assets/json_data/data_biner.json");
			$data_base_biner = json_decode($str_json);
		}

		// print_r(base_url()."assets/json_data/data_biner.json");

		// print_r($data_base_biner);

		$distict_nidn_penilaian = $this->mm->get_data_all_distict("penilaian", "nidn");

		$array_dsn = array();
		foreach ($master_dosen as $key => $value) {
			$array_dsn[$value->id_dsn] = $value;
		}

		print_r("<pre>");
		print_r("<center><h3>o> PROSES PERHITUNGAN ALTERNATIF <o</h3></center>");
		print_r("<br><hr><br><br> o> Master Kriteria <br><br>");

		print_r("<table width=\"50%\" border=\"1\">");
		print_r("	<thead>
							<tr>");
		print_r("			<th width=\"40%\">Kriteria</th>");
		print_r("			<th width=\"30%\">Nilai Minimal</th>");
		print_r("			<th width=\"30%\">Nilai Maksimal</th>");
		// print_r("			<th width=\"35%\">Tipe Kriteria</th>");
		
		print_r("		</tr>
						</thead>");
		print_r("	<tbody>");
		foreach ($master_kriteria as $key => $value) {
			$str_tipe = "cost";
			if($value->tipe_kri){
				$str_tipe = "benefit";
			}
			print_r("	<tr>");
			print_r("		<td align=\"center\">".$value->id_kri." - ".$value->ket_kri."</td>
							<td align=\"center\">".$value->min."</td>
							<td align=\"center\">".$value->max."</td>");
			print_r("	</tr>");
		}
		print_r("	<tr>");
		print_r("		<td align=\"center\">Target Nilai</td>
						<td align=\"center\">".$data_range_hasil["min"]."</td>
						<td align=\"center\">".$data_range_hasil["max"]."</td>");
		print_r("	</tr>");
		print_r("	</tbody>");
		print_r("</table>");
		// print_r("------------------------master_kriteria--------------------------");
		// print_r($master_kriteria);
		// print_r("------------------------master_kriteria--------------------------");
		// print_r($master_dosen);
		// print_r("------------------------master_kriteria--------------------------");
		// print_r($distict_nidn_penilaian);


		#------------------------------------penilaian-------------------------
			$array_kriteria = array();
			$no_first = 0;
			foreach ($master_kriteria as $r_master_kriteria => $v_master_kriteria) {
				// $array_kriteria[$v_master_kriteria->id_kri] = $v_master_kriteria; 
				$array_kriteria[$no_first] = $v_master_kriteria; 
				$no_first++;
			}
						
			$array_penilaian_row = array();
			$array_penilaian_col = array();

		$array_hasil_penilaian = array();

		if(count($master_kriteria) >= 2){
			foreach ($distict_nidn_penilaian as $r_master_dosen => $v_master_dosen) {


				#------------------------------------------data master-----------------------------#
					print_r("<hr><br><br><br>");
					

					print_r("<br><hr><br><br> o> Perhitungan ".$v_master_dosen->nidn." <br><br>");
					print_r("<table width=\"50%\" border=\"1\">");
					print_r("	<thead>
									<tr>
									<tr>
										<th colspan=\"6\">Perhitungan variabel kriteria (No. Karyawan: ".$v_master_dosen->nidn.")</th>
									</tr>");
					print_r("			<th width=\"25%\">Kriteria</th>");
					print_r("			<th width=\"15%\">Nilai Penilaian</th>");
					print_r("			<th width=\"15%\">Nilai Minimal</th>");
					print_r("			<th width=\"15%\">Nilai Makseimal</th>");
					print_r("			<th width=\"15%\">Variabel Terendah</th>");
					print_r("			<th width=\"15%\">Variabel Tertinggi</th>");
					// print_r("			<th width=\"35%\">Tipe Kriteria</th>");
					
					print_r("		</tr>
									</thead>");
					print_r("	<tbody>");
					

					$array_perhitungan_var_fuzzy = array();
					$no_kri = 0; 
					// print_r($v_master_dosen);
					foreach ($array_kriteria as $key => $value) {
							// print_r($value);
						$data_penilaian  = $this->mm->get_full_penilaian(array("nidn"=>$v_master_dosen->nidn, "p.id_kri"=>$value->id_kri));

						$param_min = 0;
						$param_max = 0;

						if((double)$data_penilaian[0]->penilaian <= (double)$value->min){
							$param_min = 1;
							$param_max = 0;
						}elseif ((double)$data_penilaian[0]->penilaian > (double)$value->min && (double)$data_penilaian[0]->penilaian < (double)$value->max) {
							$param_min = ((double)$value->max - (double)$data_penilaian[0]->penilaian)/((double)$value->max-(double)$value->min);
							$param_max = ((double)$data_penilaian[0]->penilaian - (double)$value->min)/((double)$value->max-(double)$value->min);
						}elseif ((double)$data_penilaian[0]->penilaian >= (double)$value->max) {
							$param_min = 0;
							$param_max = 1;
						}
							print_r("		<tr>");
							print_r("			<td>".$value->ket_kri."</td>");
							print_r("			<td align=\"right\">".$data_penilaian[0]->penilaian."</td>");
							print_r("			<td align=\"right\">".$value->min."</td>");
							print_r("			<td align=\"right\">".$value->max."</td>");
							print_r("			<td align=\"right\">".number_format($param_min, 3, ".", ",")."</td>");
							print_r("			<td align=\"right\">".number_format($param_max, 3, ".", ",")."</td>");
							print_r("		</tr>");
							// print_r($data_penilaian);	

						$array_perhitungan_var_fuzzy[$no_kri] = array("min"=>$param_min, "max"=>$param_max);
						$no_kri++;
					}
					print_r("	</tbody>");	
					print_r("</table>");	
					print_r("<br><br>");	
				#------------------------------------------data master-----------------------------#

				// print_r("<pre>");
				// print_r($array_perhitungan_var_fuzzy);

				#------------------------------------------data main rules-----------------------------#

					print_r("<table width=\"50%\" border=\"1\">");
					print_r("	<thead>
									<tr>
									<tr>
										<th colspan=\"".($count_kriteria+2)."\">Rules (No. Karyawan: ".$v_master_dosen->nidn.")</th>
									</tr>
											<th width=\"".(100/($count_kriteria+2))."%\">No. Rule</th>");

					foreach ($master_kriteria as $k_master_kriteria => $v_master_kriteria) {
						print_r("			<th width=\"".(100/($count_kriteria+2))."%\">".$v_master_kriteria->ket_kri."		</th>");
					}
						print_r("			<th width=\"".(100/($count_kriteria+2))."%\">Kesimpulan</th>");				
					print_r("		</tr>
									</thead>");
					print_r("	<tbody>");
					// print_r("<br><br>");

					$no_x = 1;
					$boolean_rules_con = array();
					for ($r=0; $r < $pow_kriteria ; $r++) { 
						$count_terendah = 0;
						$count_tertinggi = 0;

						print_r("	<tr>");
						print_r("		<td align=\"center\">".$no_x."</td>");
						for ($c=0; $c < $count_kriteria ; $c++) { 
							$str_biner = "Terendah";
							// $bol_biner = "false";
							if($data_base_biner[$r][$c] == 1){
								$str_biner = "Tertinggi";
								// $bol_biner = "true";
							}

							if($str_biner == "Terendah"){
								$count_terendah += 1;
							}

							if($str_biner == "Tertinggi"){
								$count_tertinggi += 1;
							}

							
							print_r("	<td align=\"center\">".$str_biner."</td>");
						}

						// print_r($count_terendah." - ".$count_tertinggi);

						$str_conlution_biner = "Tertinggi";
						if($count_terendah >= $count_tertinggi){
							$str_conlution_biner = "Terendah";
						}

						$boolean_rules_con[$r] = $str_conlution_biner;

						print_r("	<td align=\"center\">".$str_conlution_biner."</td>");
						print_r("	</tr>");

						// print_r("");
						$no_x++;
					}

					// print_r($boolean_rules_con);
					print_r("	</tbody>");
					print_r("</table>");
					print_r("<br><br>");

				#------------------------------------------data main rules-----------------------------#


				#------------------------------------------data value rules-----------------------------#

					print_r("<table width=\"50%\" border=\"1\">");
					print_r("	<thead>
									<tr>
									<tr>
										<th colspan=\"".($count_kriteria+1)."\">Nilai Rules (No. Karyawan: ".$v_master_dosen->nidn.")</th>
									</tr>
											<th width=\"".(100/($count_kriteria+1))."%\">No. Rule</th>");
					foreach ($master_kriteria as $k_master_kriteria => $v_master_kriteria) {
						print_r("			<th width=\"".(100/($count_kriteria+1))."%\">".$v_master_kriteria->ket_kri."</th>");
					}
					
					print_r("		</tr>
									</thead>");
					print_r("	<tbody>");
					// print_r("<br><br>");
					$array_nilai_rules = array();
					$array_nilai_rules_min = array();

					$no_x = 1;
					for ($r=0; $r < $pow_kriteria ; $r++) { 
						$array_nilai_rules_tmp = array();

						print_r("	<tr>");
						print_r("		<td align=\"center\">".$no_x."</td>");
						for ($c=0; $c < $count_kriteria ; $c++) { 
							$str_biner = "Terendah";
							$val_nilai_rules = $array_perhitungan_var_fuzzy[$c]["min"];

							if($data_base_biner[$r][$c] == 1){
								$str_biner = "Tertinggi";
								$val_nilai_rules = $array_perhitungan_var_fuzzy[$c]["max"];
							}

							$array_nilai_rules_tmp[$c] = $val_nilai_rules;

							$array_nilai_rules[$r][$c] = number_format($val_nilai_rules, 3, ".", ",");
							print_r("	<td align=\"center\">".number_format($val_nilai_rules, 3, ".", ",")."</td>");
						}
						$array_nilai_rules_min[$r] = min($array_nilai_rules_tmp);
						print_r("	</tr>");
						$no_x++;
					}
					print_r("	</tbody>");
					print_r("</table>");
					print_r("<br><br>");

				#------------------------------------------data value rules-----------------------------#

				// print_r($array_perhitungan_var_fuzzy);
				// print_r($array_nilai_rules_min);
				// print_r($array_kriteria);

				

				// print_r($data_range_hasil);

				#------------------------------------------data hasil-----------------------------#

					$array_z = array();
					foreach ($array_nilai_rules_min as $k_array_nilai_rules_min => $v_array_nilai_rules_min) {
						if($boolean_rules_con[$k_array_nilai_rules_min] == "Tertinggi"){
							$array_z[$k_array_nilai_rules_min] = $array_nilai_rules_min[$k_array_nilai_rules_min] * ($data_range_hasil["max"] - $data_range_hasil["min"]) + $data_range_hasil["min"];

						}elseif ($boolean_rules_con[$k_array_nilai_rules_min] == "Terendah") {
							$array_z[$k_array_nilai_rules_min] = $data_range_hasil["max"] - ($data_range_hasil["max"] - $data_range_hasil["min"]) * $array_nilai_rules_min[$k_array_nilai_rules_min];
						}
					}

					// print_r($array_z);


					print_r("<table width=\"50%\" border=\"1\">");
					print_r("	<thead>
									<tr>
									<tr>
										<th colspan=\"5\">Perhitungan (No. Karyawan: ".$v_master_dosen->nidn.")</th>
									</tr>
											<th width=\"10%\">No. Rule</th>
											<th width=\"30%\">Nilai Minimal Rule</th>
											<th width=\"30%\">Nilai Z Rule</th>
											<th width=\"30%\">Nilai Z*Minimal Rule</th>");
					
					print_r("		</tr>
									</thead>");
					print_r("	<tbody>");
					// print_r("<br><br>");

					$no_x = 1;
					$array_min_z = array();
					foreach ($array_z as $k_array_z => $v_array_z) {
						$val_min_z = $array_nilai_rules_min[$k_array_z]*$array_z[$k_array_z];
						$array_min_z[$k_array_z] = $val_min_z;

						print_r("		<tr>
											<td align=\"center\">".$no_x."</td>
											<td align=\"center\">".number_format($array_nilai_rules_min[$k_array_z], 3, ".", ",")."</td>
											<td align=\"center\">".number_format($array_z[$k_array_z], 3, ".", ",")."</td>
											<td align=\"center\">".number_format($val_min_z, 3, ".", ",")."</td>
										</tr>");
						$no_x++;
					}

					// print_r($array_min_z);

					$sum_array_nilai_rules_min = array_sum($array_nilai_rules_min);
					$sum_array_min_z = array_sum($array_min_z);

					// print_r($sum_array_nilai_rules_min);
					// print_r("<br>");
					// print_r($sum_array_min_z);
					// print_r("<br>");

					print_r("			<tr>
											<td align=\"center\">-</td>
											<td align=\"center\">".number_format($sum_array_nilai_rules_min, 3, ".", ",")."</td>
											<td align=\"center\">-</td>
											<td align=\"center\">".number_format($sum_array_min_z, 3, ".", ",")."</td>
										</tr>");


					$hasil = $sum_array_min_z / $sum_array_nilai_rules_min;

					$array_hasil_penilaian[$r_master_dosen] = array("nidn"=>$v_master_dosen->nidn, "hasil"=>number_format($hasil, 3, ".", ","));

					// print_r($v_master_dosen);
					// $array_hasil_penilaian[$r_master_dosen] = array("nidn"=>$v_master_dosen->nidn,
					// 											"nama"=>$v_master_dosen->nama,
					// 											"hasil"=>$hasil);
					

					print_r("			<tr>
											<td align=\"center\" colspan=\"3\">Hasil Penilaian</td>
											<td align=\"center\">".number_format($hasil, 3, ".", ",")."</td>
										</tr>");
					// print_r($hasil);



					print_r("	</tbody>");
					print_r("</table>");
					print_r("<br><br>");
				#------------------------------------------data hasil-----------------------------#
			}

			print_r("<pre>");
			print_r("<br><hr><br><br>");
			print_r("<center><h3>o> HASIL PERHITUNGAN ALTERNATIF <o</h3></center>");
			print_r("<br><hr><br><br>");


			print_r("<table width=\"50%\" border=\"1\">");
			print_r("	<thead>
							<tr>
								<th colspan=\"2\">Hasil Perhitungan</th>
							</tr>
							<tr>
								<th width=\"50%\">No. Karyawan</th>
								<th width=\"50%\"></th>");
					
			print_r("		</tr>
						</thead>");
			print_r("	<tbody>");
			foreach ($array_hasil_penilaian as $k_array_hasil_penilaian => $v_array_hasil_penilaian) {
				print_r("	<tr>
								<td align=\"center\">".$v_array_hasil_penilaian["nidn"]."</td>
								<td align=\"center\">".$v_array_hasil_penilaian["hasil"]."</td>	
							</tr>");
			}
			print_r("	</tbody>");
			print_r("</table>");
			print_r("<br><br>");
		}else{
			print_r("<br><br><hr>");
			print_r("<br><br>");
			print_r("Aktifkan lebih 2 atau lebih krieria untuk melakukan proses");
		}	
		// print_r($array_hasil_penilaian);
	}

	public function index_show_result(){
		$master_kriteria = $this->mm->get_data_all_where("kriteria", array("sts_active"=>"0"));
		$master_dosen = $this->mm->get_data_all("dsn");
		$data_range_hasil = $this->mm->get_data_each("range_hasil", array());

		$count_kriteria = count($master_kriteria);
		$pow_kriteria = pow(2, $count_kriteria);

		$data["hasil"] = array();

		if(count($master_kriteria) >= 2){

			$delete_dummy_hasil = $this->db->query("DELETE FROM dummy_hasil WHERE 1");

			$data_base_biner = array();
			if(file_exists("./assets/json_data/data_biner.json")){
				$str_json = file_get_contents("./assets/json_data/data_biner.json");
				$data_base_biner = json_decode($str_json);
			}


			$distict_nidn_penilaian = $this->mm->get_data_all_distict("penilaian", "nidn");

			$array_dsn = array();
			foreach ($master_dosen as $key => $value) {
				$array_dsn[$value->id_dsn] = $value;
			}

			foreach ($master_kriteria as $key => $value) {
				$str_tipe = "cost";
				if($value->tipe_kri){
					$str_tipe = "benefit";
				}
				
			}
			


			#------------------------------------penilaian-------------------------
				$array_kriteria = array();
				$no_first = 0;
				foreach ($master_kriteria as $r_master_kriteria => $v_master_kriteria) {
					// $array_kriteria[$v_master_kriteria->id_kri] = $v_master_kriteria; 
					$array_kriteria[$no_first] = $v_master_kriteria; 
					$no_first++;
				}
							
				$array_penilaian_row = array();
				$array_penilaian_col = array();

			$array_hasil_penilaian = array();

			foreach ($distict_nidn_penilaian as $r_master_dosen => $v_master_dosen) {


				#------------------------------------------data master-----------------------------#

					$array_perhitungan_var_fuzzy = array();
					$no_kri = 0; 
					// print_r($v_master_dosen);
					foreach ($array_kriteria as $key => $value) {
							// print_r($value);
						$data_penilaian  = $this->mm->get_full_penilaian(array("nidn"=>$v_master_dosen->nidn, "p.id_kri"=>$value->id_kri));

						$param_min = 0;
						$param_max = 0;

						if((double)$data_penilaian[0]->penilaian <= (double)$value->min){
							$param_min = 1;
							$param_max = 0;
						}elseif ((double)$data_penilaian[0]->penilaian > (double)$value->min && (double)$data_penilaian[0]->penilaian < (double)$value->max) {
							$param_min = ((double)$value->max - (double)$data_penilaian[0]->penilaian)/((double)$value->max-(double)$value->min);
							$param_max = ((double)$data_penilaian[0]->penilaian - (double)$value->min)/((double)$value->max-(double)$value->min);
						}elseif ((double)$data_penilaian[0]->penilaian >= (double)$value->max) {
							$param_min = 0;
							$param_max = 1;
						}	

						$array_perhitungan_var_fuzzy[$no_kri] = array("min"=>$param_min, "max"=>$param_max);
						$no_kri++;
					}
				#------------------------------------------data master-----------------------------#


				#------------------------------------------data main rules-----------------------------
					$no_x = 1;
					$boolean_rules_con = array();
					for ($r=0; $r < $pow_kriteria ; $r++) { 
						$count_terendah = 0;
						$count_tertinggi = 0;

						for ($c=0; $c < $count_kriteria ; $c++) { 
							$str_biner = "Terendah";
							// $bol_biner = "false";
							if($data_base_biner[$r][$c] == 1){
								$str_biner = "Tertinggi";
								// $bol_biner = "true";
							}

							if($str_biner == "Terendah"){
								$count_terendah += 1;
							}

							if($str_biner == "Tertinggi"){
								$count_tertinggi += 1;
							}
						}


						$str_conlution_biner = "Tertinggi";
						if($count_terendah >= $count_tertinggi){
							$str_conlution_biner = "Terendah";
						}

						$boolean_rules_con[$r] = $str_conlution_biner;

						$no_x++;
					}


				#------------------------------------------data main rules-----------------------------#


				#------------------------------------------data value rules-----------------------------#

					// print_r("<br><br>");
					$array_nilai_rules = array();
					$array_nilai_rules_min = array();

					$no_x = 1;
					for ($r=0; $r < $pow_kriteria ; $r++) { 
						$array_nilai_rules_tmp = array();

						for ($c=0; $c < $count_kriteria ; $c++) { 
							$str_biner = "Terendah";
							$val_nilai_rules = $array_perhitungan_var_fuzzy[$c]["min"];

							if($data_base_biner[$r][$c] == 1){
								$str_biner = "Tertinggi";
								$val_nilai_rules = $array_perhitungan_var_fuzzy[$c]["max"];
							}

							$array_nilai_rules_tmp[$c] = $val_nilai_rules;

							$array_nilai_rules[$r][$c] = number_format($val_nilai_rules, 3, ".", ",");
							
						}
						$array_nilai_rules_min[$r] = min($array_nilai_rules_tmp);
						$no_x++;
					}
				#------------------------------------------data value rules-----------------------------#



				#------------------------------------------data hasil-----------------------------#

					$array_z = array();
					foreach ($array_nilai_rules_min as $k_array_nilai_rules_min => $v_array_nilai_rules_min) {
						if($boolean_rules_con[$k_array_nilai_rules_min] == "Tertinggi"){
							$array_z[$k_array_nilai_rules_min] = $array_nilai_rules_min[$k_array_nilai_rules_min] * ($data_range_hasil["max"] - $data_range_hasil["min"]) + $data_range_hasil["min"];

						}elseif ($boolean_rules_con[$k_array_nilai_rules_min] == "Terendah") {
							$array_z[$k_array_nilai_rules_min] = $data_range_hasil["max"] - ($data_range_hasil["max"] - $data_range_hasil["min"]) * $array_nilai_rules_min[$k_array_nilai_rules_min];
						}
					}

					$no_x = 1;
					$array_min_z = array();
					foreach ($array_z as $k_array_z => $v_array_z) {
						$val_min_z = $array_nilai_rules_min[$k_array_z]*$array_z[$k_array_z];
						$array_min_z[$k_array_z] = $val_min_z;
						$no_x++;
					}

					$sum_array_nilai_rules_min = array_sum($array_nilai_rules_min);
					$sum_array_min_z = array_sum($array_min_z);

					$hasil = $sum_array_min_z / $sum_array_nilai_rules_min;


					$insert_dummy_hasil = $this->mm->insert_data("dummy_hasil", array("nidn"=>$v_master_dosen->nidn, "hasil"=>number_format($hasil, 3, ".", ",")));

					$array_hasil_penilaian[$r_master_dosen] = array("nidn"=>$v_master_dosen->nidn, "hasil"=>number_format($hasil, 3, ".", ","));
				#------------------------------------------data hasil-----------------------------#
			}

			$data["hasil"] = $this->mm->get_hasil();
		}

		$data["page"] = "page_hasil";
		
		// print_r("<pre>");
		// print_r($data);
		$this->load->view("index", $data);

	}

	
}
