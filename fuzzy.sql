-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Jun 2019 pada 01.19
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fuzzy`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_dsn`(`nama` TEXT, `nidn` VARCHAR(32), `alamat` TEXT, `tlp` VARCHAR(13)) RETURNS varchar(12) CHARSET latin1
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from dsn;
        
  select id_dsn into last_key_item from dsn order by id_dsn desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("D","10000001");
  else
    set fix_key_item = concat("D",RIGHT(last_key_item,8)+1);
      
  END IF;
  
  insert into dsn values(fix_key_item, nidn, nama, alamat, tlp);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_key_kri`(`ket_kri` VARCHAR(50), `tipe_kri` ENUM('0','1'), `min_x` VARCHAR(50), `max_x` VARCHAR(50)) RETURNS varchar(20) CHARSET latin1
BEGIN
  declare last_key_kri varchar(20);
  select id_kri into last_key_kri from kriteria order by id_kri desc limit 1;
  
  insert into kriteria values(last_key_kri+1, ket_kri, tipe_kri, '0', min_x, max_x, '0');
 
  
  return last_key_kri+1;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_key_sub_kri`(`id_kri` INT(20), `ket_sub` VARCHAR(64), `val_sub` INT(20)) RETURNS varchar(20) CHARSET latin1
BEGIN
  declare last_key_sub_kri varchar(20);
  select id_sub_kri into last_key_sub_kri from kriteria_sub order by id_sub_kri desc limit 1;
  
  insert into kriteria_sub values(last_key_sub_kri+1, id_kri, ket_sub, val_sub);
 
  
  return last_key_sub_kri+1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `password`, `nama`, `nip`, `jabatan`) VALUES
('AD20180912', 'admin', 'aff8fbcbf1363cd7edc85a1e11391173', 'Surya Hanggara', '081230695774', 'Pengawas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dsn`
--

CREATE TABLE IF NOT EXISTS `dsn` (
  `id_dsn` varchar(10) NOT NULL,
  `nidn` varchar(10) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `tlp` varchar(13) NOT NULL,
  PRIMARY KEY (`id_dsn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dsn`
--

INSERT INTO `dsn` (`id_dsn`, `nidn`, `nama`, `alamat`, `tlp`) VALUES
('D10000001', '1231009871', 'sudirmans', 'malangs', '0812306957451'),
('D10000002', '1231009872', 'Dr. Radjiman Setyo', 'bantul', '081564879354'),
('D10000003', '1231009871', 'Darmono', 'donomulyo', '085654123987'),
('D10000004', '2132312321', 'dono', 'situbondo', '321656158987'),
('D10000005', '2153652176', 'surya ok', 'malang', '081283984'),
('D10000006', '12151416', 'surya hanggara', 'Malang', '081230695774');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dummy_hasil`
--

CREATE TABLE IF NOT EXISTS `dummy_hasil` (
  `nidn` varchar(64) NOT NULL,
  `hasil` varchar(64) NOT NULL,
  PRIMARY KEY (`nidn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dummy_hasil`
--

INSERT INTO `dummy_hasil` (`nidn`, `hasil`) VALUES
('D10000001', '7.818'),
('D10000002', '10.000'),
('D10000003', '10.000'),
('D10000004', '8.000'),
('D10000006', '8.270');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriteria`
--

CREATE TABLE IF NOT EXISTS `kriteria` (
  `id_kri` int(2) NOT NULL AUTO_INCREMENT,
  `ket_kri` varchar(64) NOT NULL,
  `tipe_kri` enum('0','1') NOT NULL,
  `bobot` int(2) NOT NULL,
  `min` varchar(50) NOT NULL,
  `max` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_kri`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data untuk tabel `kriteria`
--

INSERT INTO `kriteria` (`id_kri`, `ket_kri`, `tipe_kri`, `bobot`, `min`, `max`, `sts_active`) VALUES
(14, 'tes', '0', 0, '60', '100', '0'),
(15, 'mantul', '1', 0, '60', '100', '0'),
(16, 'tes', '1', 0, '0', '4', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriteria_sub`
--

CREATE TABLE IF NOT EXISTS `kriteria_sub` (
  `id_sub_kri` int(11) NOT NULL AUTO_INCREMENT,
  `id_kri` int(11) NOT NULL,
  `ket_sub` varchar(64) NOT NULL,
  `val_sub` int(11) NOT NULL,
  PRIMARY KEY (`id_sub_kri`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data untuk tabel `kriteria_sub`
--

INSERT INTO `kriteria_sub` (`id_sub_kri`, `id_kri`, `ket_sub`, `val_sub`) VALUES
(51, 9, '1 - 3 th', 1),
(52, 10, '<= 2 bahasa', 1),
(53, 11, 'sarjanah', 1),
(54, 9, '4 - 7 th', 2),
(55, 9, '> 8 th', 3),
(56, 10, '3 - 5 bahasa', 2),
(57, 10, '>= 6 bahasa', 3),
(58, 11, 'magister', 2),
(59, 11, 'doctor', 3),
(60, 11, 'profesor', 4),
(62, 12, 'dummy', 1),
(63, 9, 'ojo', 3),
(64, 13, 'sub surya 1', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penilaian`
--

CREATE TABLE IF NOT EXISTS `penilaian` (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `nidn` varchar(10) NOT NULL,
  `id_kri` int(11) NOT NULL,
  `penilaian` varchar(64) NOT NULL,
  PRIMARY KEY (`id_nilai`),
  KEY `id_kri` (`id_kri`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=140 ;

--
-- Dumping data untuk tabel `penilaian`
--

INSERT INTO `penilaian` (`id_nilai`, `nidn`, `id_kri`, `penilaian`) VALUES
(95, 'D10000001', 14, '75'),
(96, 'D10000001', 15, '80'),
(97, 'D10000001', 16, '1'),
(99, 'D10000003', 14, '100'),
(100, 'D10000003', 15, '100'),
(101, 'D10000003', 16, '100'),
(102, 'D10000002', 14, '10'),
(103, 'D10000002', 15, '100'),
(104, 'D10000002', 16, '50'),
(128, 'D10000004', 14, '55'),
(129, 'D10000004', 15, '75'),
(130, 'D10000004', 16, '2'),
(137, 'D10000006', 14, '100'),
(138, 'D10000006', 15, '89'),
(139, 'D10000006', 16, '3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `range_hasil`
--

CREATE TABLE IF NOT EXISTS `range_hasil` (
  `id_range` int(11) NOT NULL AUTO_INCREMENT,
  `min` varchar(20) NOT NULL,
  `max` varchar(20) NOT NULL,
  PRIMARY KEY (`id_range`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `range_hasil`
--

INSERT INTO `range_hasil` (`id_range`, `min`, `max`) VALUES
(1, '6', '10');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
